package fr.avatarreturns.avatarhouse.messages;

import fr.avatarreturns.avatarhouse.configuration.Config;
import fr.avatarreturns.avatarhouse.permissions.Permissions;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Messages {

    PREFIX_INFO("&3&lAvatarHouses &8&l» &r"),
    PREFIX_ERROR("&c&lAvatarHouses &4&l» &r"),

    INFO_UPDATED("%prefix_info% &7Instance mise à jour"),
    INFO_GOTO("%prefix_info% &7Vous devez aller à l'agent immobilier du voisinage de cette maison."),
    INFO_AVAILABLE("%prefix_info% &7La maison {0} est de nouveau disponible."),

    ERROR_REGION_INVALID("%prefix_error% &cVous devez rentrer le nom d'une région WorldGuard valide."),
    ERROR_REGION_FOUND("%prefix_error% &cCette zone n'appartient pas à cette maison."),
    ERROR_REGION_EXISTS("%prefix_error% &cCette zone appartient déjà à une maison."),
    ERROR_FIRST_SIGN("%prefix_error% &cVous devez d'abord renseigner un panneau : &4/houses edit house {0} location&c."),
    ERROR_SIGN("%prefix_error% &cVous devez viser un panneau."),
    ERROR_INVALID("%prefix_error% &cValeur invalide."),
    ERROR_MONEY("%prefix_error% &cVous n'avez pas assez d'argent."),
    ERROR_OWNER("%prefix_error% &cVous n'êtes pas le propriétaire de cette maison.");

    private String message;

    Messages(final String message) {
        this.message = message;
    }

    public String getFullRaw() {
        return this.message;
    }

    public String get(final String... arguments) {
        String result = this.message;
        for (int i = 0; i < arguments.length; i++)
            result = result.replace("{" + i + "}", arguments[i]);
        return ChatColor.translateAlternateColorCodes('&', result
                .replace("%prefix_info%", PREFIX_INFO.message)
                .replace("%prefix_error%", PREFIX_ERROR.message)
                .replace("¤", " ")
        );
    }

    public static void init(final String path, final String name) {
        final File file = new File(path, name);
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
            for (final Messages messages : Messages.values()) {
                if (messages.message.equals(""))
                    continue;
                final String pathToMessage = messages.name().replace("__", "-").replace("_", ".").toLowerCase();
                if (!Config.addDefault(config, pathToMessage, messages.message)) {
                    messages.message = config.getString(pathToMessage);
                }
            }
            config.save(file);
        } catch (Exception ignored) {
        }
    }


}
