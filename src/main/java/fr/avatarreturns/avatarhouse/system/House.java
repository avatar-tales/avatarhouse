package fr.avatarreturns.avatarhouse.system;

import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.inventory.InventoryAPI;
import fr.avatarreturns.api.users.IUser;
import fr.avatarreturns.avatarhouse.configuration.Config;
import fr.avatarreturns.avatarhouse.configuration.GsonSerializer;
import fr.avatarreturns.avatarhouse.messages.Messages;
import fr.avatarreturns.avatarhouse.plugin.AvatarHouse;
import fr.avatarreturns.avatarhouse.utils.Utils;
import net.minecraft.server.v1_16_R3.SoundEffects;
import net.wesjd.anvilgui.AnvilGUI;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class House {

    private static transient final List<House> houses = new ArrayList<>();

    private final UUID uniqueId;
    private final List<String> regions;
    private final List<UUID> co_owners;
    private final List<UUID> members;
    @Parameter("setName")
    private String name;
    @Parameter("setPrice")
    private double price;
    @Parameter("setRent")
    private double rent;
    @Parameter("setRentTime")
    private long rentTime;
    private long lastRentTime;
    @Parameter("setReset")
    private boolean reset;
    @Parameter("setCoOwnerPrice")
    private double coOwnerPrice;
    @Parameter("setMemberPrice")
    private double memberPrice;
    @Parameter("setStrangerToMember")
    private double strangerToMember;
    @Parameter("setMemberToCoOwner")
    private double memberToCoOwner;
    @Parameter("setCoOwnerToMember")
    private double coOwnerToMember;
    @Parameter("setMemberToStranger")
    private double memberToStranger;
    @Parameter("setLeave")
    private double leave;
    private Location sign;
    private State state;
    private UUID owner;

    public House(final String name) {
        this.uniqueId = UUID.randomUUID();
        this.name = name;
        this.price = Config.getHouse_default_price();
        this.rent = Config.getHouse_default_rent();
        this.rentTime = Config.getHouse_default_rentTime();
        this.lastRentTime = System.currentTimeMillis();
        this.coOwnerPrice = Config.getHouse_default_coowner();
        this.memberPrice = Config.getHouse_default_member();
        this.strangerToMember = Config.getHouse_default_strangerToMember();
        this.memberToCoOwner = Config.getHouse_default_memberToCoOwner();
        this.coOwnerToMember = Config.getHouse_default_coOwnerToMember();
        this.memberToStranger = Config.getHouse_default_memberToStranger();
        this.leave = Config.getHouse_default_leave();
        this.reset = Config.isHouse_default_reset();
        this.sign = null;
        this.regions = new ArrayList<>();
        this.state = State.INVALID;
        this.owner = null;
        this.co_owners = new ArrayList<>();
        this.members = new ArrayList<>();
    }

    public static Optional<House> getHouse(final UUID uniqueId) {
        return houses.stream().filter(house -> house.getUniqueId().equals(uniqueId)).findFirst();
    }

    public static Optional<House> getHouse(final String name) {
        return houses.stream().filter(house -> house.getRealName().equals(name)).findFirst();
    }

    public static List<House> getHouses() {
        return houses;
    }

    public static void save(final House house) {
        AvatarReturnsAPI.get().debug("Saving house : " + house.getUniqueId() + " ...", AvatarHouse.get());
        final File file = new File(AvatarHouse.get().getHouseSavePath(), house.getUniqueId() + ".json");
        try {
            if (!file.exists()) {
                AvatarReturnsAPI.get().debug("... creating file ...", AvatarHouse.get());
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            try (FileWriter output = new FileWriter(file)) {
                AvatarReturnsAPI.get().debug("... writing file ...", AvatarHouse.get());
                output.write(GsonSerializer.get().getGson().toJson(house));
            } catch (IOException e) {
                AvatarReturnsAPI.get().debug("... FAILED.", AvatarHouse.get());
                e.printStackTrace();
                return;
            }
        } catch (IOException e) {
            AvatarReturnsAPI.get().debug("... FAILED.", AvatarHouse.get());
            e.printStackTrace();
            return;
        }
        AvatarReturnsAPI.get().debug("... SUCCESS.", AvatarHouse.get());
    }

    public static Optional<House> load(final File file) {
        AvatarReturnsAPI.get().debug("Loading house " + file.getName() + "...", AvatarHouse.get());
        if (!file.exists()) {
            AvatarReturnsAPI.get().debug("... not found ...", AvatarHouse.get());
            AvatarReturnsAPI.get().debug("... FAILED.", AvatarHouse.get());
            return Optional.empty();
        }
        try (final FileReader input = new FileReader(file)) {
            AvatarReturnsAPI.get().debug("... reading file ...", AvatarHouse.get());
            final House house = GsonSerializer.get().getGson().fromJson(input, House.class);
            if (house == null) {
                AvatarReturnsAPI.get().debug("... FAILED.", AvatarHouse.get());
                return Optional.empty();
            }
            AvatarReturnsAPI.get().debug("... SUCCESS.", AvatarHouse.get());
            return Optional.of(house);
        } catch (IOException e) {
            AvatarReturnsAPI.get().debug("... FAILED.", AvatarHouse.get());
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public static List<Optional<House>> load(final String directory) {
        final File file = new File(directory);
        if (!file.exists())
            return new ArrayList<>();
        return Arrays.stream(file.listFiles()).map(House::load).collect(Collectors.toList());
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    private String getRealName() {
        return this.name;
    }

    public String getName() {
        return this.name.replace("_", " ");
    }

    public void setName(final String name) {
        this.name = name;
        this.isInvalid();
        this.save();
        this.updateSign();
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(final double price) {
        this.price = price;
        this.isInvalid();
        this.save();
        this.updateSign();
    }

    public double getRent() {
        return this.rent;
    }

    public void setRent(final double rent) {
        this.rent = rent;
        this.isInvalid();
        this.save();
        this.updateSign();
    }

    public long getRentTime() {
        return this.rentTime;
    }

    public void setRentTime(final long rentTime) {
        this.rentTime = rentTime;
        this.isInvalid();
        this.save();
    }

    public long getLastRentTime() {
        return lastRentTime;
    }

    public void setLastRentTime(final long lastRentTime) {
        this.lastRentTime = lastRentTime;
        this.isInvalid();
        this.save();
    }

    public boolean isReset() {
        return this.reset;
    }

    public void setReset(boolean reset) {
        this.reset = reset;
        this.isInvalid();
        this.save();
    }

    public double getCoOwnerPrice() {
        return coOwnerPrice;
    }

    public void setCoOwnerPrice(final double coOwnerPrice) {
        this.coOwnerPrice = coOwnerPrice;
        this.save();
    }

    public double getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(final double memberPrice) {
        this.memberPrice = memberPrice;
        this.save();
    }

    public double getStrangerToMember() {
        return strangerToMember;
    }

    public void setStrangerToMember(final double strangerToMember) {
        this.strangerToMember = strangerToMember;
        this.save();
    }

    public double getMemberToCoOwner() {
        return memberToCoOwner;
    }

    public void setMemberToCoOwner(final double memberToCoOwner) {
        this.memberToCoOwner = memberToCoOwner;
        this.save();
    }

    public double getCoOwnerToMember() {
        return coOwnerToMember;
    }

    public void setCoOwnerToMember(final double coOwnerToMember) {
        this.coOwnerToMember = coOwnerToMember;
        this.save();
    }

    public double getMemberToStranger() {
        return memberToStranger;
    }

    public void setMemberToStranger(final double memberToStranger) {
        this.memberToStranger = memberToStranger;
        this.save();
    }

    public double getLeave() {
        return leave;
    }

    public void setLeave(double leave) {
        this.leave = leave;
        this.save();
    }

    public Optional<Location> getSign() {
        if (this.sign == null)
            return Optional.empty();
        if (!Utils.isSign(this.sign.convert().getBlock().getType()))
            return Optional.empty();
        return Optional.ofNullable(this.sign);
    }

    public void setSign(Location sign) {
        this.sign = sign;
        this.isInvalid();
        this.save();
        this.updateSign();
    }

    public List<String> getRegions() {
        return this.regions;
    }

    public State getState() {
        return this.state;
    }

    public void setState(State state) {
        this.state = state;
        this.isInvalid();
        this.save();
        this.updateSign();
    }

    public Optional<UUID> getOwner() {
        return Optional.ofNullable(this.owner);
    }

    public void setOwner(final UUID owner) {
        if (owner == null) {
            this.getMembers().clear();
            this.getCo_owners().clear();
            Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(Messages.INFO_AVAILABLE.get(getName())));
        }
        this.owner = owner;
        this.isInvalid();
        this.save();
        this.updateSign();
        this.updateRegions();
    }

    public List<UUID> getCo_owners() {
        return this.co_owners;
    }

    public List<UUID> getMembers() {
        return this.members;
    }

    public boolean isInvalid() {
        if (this.sign == null || !Utils.isSign(this.sign.convert().getBlock().getType())) {
            this.state = State.INVALID;
            if (this.sign != null)
                this.sign = null;
        } else if (this.regions.size() == 0)
            this.state = State.INVALID;
        else if (this.owner == null)
            this.state = State.FREE;
        else
            this.state = State.OCCUPIED;
        return this.state == State.INVALID;
    }

    public boolean isOwner(final UUID uniqueId) {
        if (this.isInvalid())
            return false;
        if (this.owner == null)
            return false;
        return this.owner.equals(uniqueId);
    }

    public boolean isCoOwner(final UUID uniqueId) {
        return this.co_owners.contains(uniqueId);
    }

    public boolean isMember(final UUID uniqueId) {
        return this.members.contains(uniqueId);
    }

    public boolean isLink() {
        return Neighbourhood.getNeighbourhoods().stream().anyMatch(neighbourhood -> neighbourhood.getHouses().contains(this.getUniqueId()));
    }

    public void addRegion(final String name) {
        if (this.regions.contains(name))
            return;
        this.regions.add(name);
        this.updateRegions();
        this.updateSign();
    }

    public void removeRegion(final String name) {
        if (!this.regions.contains(name))
            return;
        this.regions.stream()
                .filter(region -> region.equals(name))
                .map(regionName -> Utils.getRegion(this.sign.convert().getWorld(), regionName))
                .forEach(region -> {
                    if (region == null)
                        return;
                    region.getOwners().clear();
                    region.getMembers().clear();
                });
        this.regions.remove(name);
        this.updateRegions();
        this.updateSign();
    }

    public void updateSign() {
        this.getSign().ifPresent(location -> {
            final Sign sign = (Sign) location.convert().getBlock().getState();
            sign.setEditable(true);
            sign.update();
            for (int i = 0; i < 4; i++)
                sign.setLine(i, "");
            sign.setLine(0, "§3§l" + this.getName());
            isInvalid();
            if (this.state.equals(State.OCCUPIED)) {
                sign.setLine(1, "§4OCCUPÉ");
                sign.setLine(2, "§c§lpar :");
                sign.setLine(3, "§7" + Bukkit.getOfflinePlayer(this.owner).getName());
            } else if (this.state == State.FREE) {
                sign.setLine(1, "§aLIBRE");
                sign.setLine(2, "§6§lPrix : §e" + Utils.change(Math.ceil(this.getPrice() * 100) / 100));
                sign.setLine(3, "§6§lLoyer : §e" + Utils.change(Math.ceil(this.getRent() * 100) / 100));
            } else
                sign.setLine(1, "§6En construction");
            sign.setEditable(false);
            sign.update();
        });
    }

    public void updateRegions() {
        if (this.sign == null)
            return;
        this.regions.stream().map(name -> Utils.getRegion(this.sign.convert().getWorld(), name))
                .forEach(region -> {
                    if (region == null)
                        return;
                    region.getOwners().clear();
                    region.getMembers().clear();
                    region.setFlag(Flags.MOB_SPAWNING, StateFlag.State.DENY);
                    region.setFlag(Flags.PVP, StateFlag.State.DENY);
                    region.setFlag(Flags.PISTONS, StateFlag.State.DENY);
                    if (this.owner != null)
                        region.getOwners().addPlayer(this.owner);
                    this.getCo_owners().forEach(region.getMembers()::addPlayer);
                    this.getMembers().forEach(region.getMembers()::addPlayer);
                });
    }

    public void save() {
        save(this);
    }

    public void delete() {
        getHouses().remove(this);
        this.setOwner(null);
        this.getSign().ifPresent(location -> {
            final Sign sign = (Sign) location.convert().getBlock().getState();
            sign.setEditable(true);
            for (int i = 0; i < 4; i++)
                sign.setLine(i, "");
            sign.update();
        });
        this.regions.stream()
                .map(regionName -> Utils.getRegion(this.sign.convert().getWorld(), regionName))
                .forEach(region -> {
                    if (region == null)
                        return;
                    region.getOwners().clear();
                    region.getMembers().clear();
                });
        try {
            final File file = new File(AvatarHouse.get().getHouseSavePath(), this.getUniqueId() + ".json");
            if (file.exists())
                file.delete();
            this.finalize();
        } catch (Throwable ignored) {
        }
    }

    public enum State {

        INVALID,
        OCCUPIED,
        FREE

    }

    public static class Inventories {

        public static void editHouseMenu(final House house, final Player player, final int page) {
            final InventoryAPI inv = AvatarReturnsAPI.get().getInventory(AvatarHouse.get())
                    .setRefresh(false)
                    .setSize(3 * 9)
                    .setTitle("§3§l" + house.getName() + " §8» §7Édition");
            final List<String> lore = new ArrayList<>();
            final ItemStack backward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjI1OTliZDk4NjY1OWI4Y2UyYzQ5ODg1MjVjOTRlMTlkZGQzOWZhZDA4YTM4Mjg0YTE5N2YxYjcwNjc1YWNjIn19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(backward, "§6§lPage précédente");
            final ItemStack forward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzJmOTEwYzQ3ZGEwNDJlNGFhMjhhZjZjYzgxY2Y0OGFjNmNhZjM3ZGFiMzVmODhkYjk5M2FjY2I5ZGZlNTE2In19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(forward, "§6§lPage suivante");
            if (page == 1) {
                final ItemStack name = AvatarReturnsAPI.get().getUtils().getItem(Material.NAME_TAG, "§cNom §4:");
                lore.add("  §7» §c" + house.getName());
                AvatarReturnsAPI.get().getUtils().modifyItem(name, lore);
                inv.addItem(10, name, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Édition §8» §7Input")
                            .plugin(AvatarHouse.get())
                            .text(house.getName())
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.NAME_TAG, "..."))
                            .onComplete((p, value) -> {
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                if (value.equals(house.getName()))
                                    return AnvilGUI.Response.text(house.getName());
                                p.performCommand("avatarhouse edit house \"" + house.getName() + "\" set name \"" + value + "\"");
                                return AnvilGUI.Response.close();
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> editHouseMenu(house, p, page), 2))
                            .open(player);
                });
                final ItemStack price = AvatarReturnsAPI.get().getUtils().getItem(Material.GOLD_BLOCK, "§cPrix");
                lore.clear();
                lore.add("  §7» §c" + Utils.change(Math.ceil(house.getPrice() * 100) / 100));
                AvatarReturnsAPI.get().getUtils().modifyItem(price, lore);
                inv.addItem(11, price, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Édition §8» §7Input")
                            .plugin(AvatarHouse.get())
                            .text(Utils.change(Math.ceil(house.getPrice() * 100) / 100))
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.GOLD_BLOCK, Utils.change(Math.ceil(house.getPrice() * 100) / 100)))
                            .onComplete((p, value) -> {
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                if (!isDouble(value))
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(house.getPrice() * 100) / 100));
                                final double priceValue = Double.parseDouble(value);
                                if (priceValue == house.getPrice())
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(priceValue * 100) / 100));
                                p.performCommand("avatarhouse edit house \"" + house.getName() + "\" set price " + priceValue);
                                return AnvilGUI.Response.close();
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> editHouseMenu(house, p, page), 2))
                            .open(player);
                });
                final ItemStack rent = AvatarReturnsAPI.get().getUtils().getItem(Material.GOLD_INGOT, "§cLoyer");
                lore.clear();
                lore.add("  §7» §c" + Utils.change(Math.ceil(house.getRent() * 100) / 100));
                AvatarReturnsAPI.get().getUtils().modifyItem(rent, lore);
                inv.addItem(12, rent, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Édition §8» §7Input")
                            .plugin(AvatarHouse.get())
                            .text(Utils.change(Math.ceil(house.getRent() * 100) / 100))
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.GOLD_INGOT, String.valueOf(Utils.change(Math.ceil(house.getRent() * 100) / 100))))
                            .onComplete((p, value) -> {
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                if (!isDouble(value))
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(house.getRent() * 100) / 100));
                                final double rentValue = Double.parseDouble(value);
                                if (rentValue == house.getRent())
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(rentValue * 100) / 100));
                                p.performCommand("avatarhouse edit house \"" + house.getName() + "\" set rent " + rentValue);
                                return AnvilGUI.Response.close();
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> editHouseMenu(house, p, page), 2))
                            .open(player);
                });
                if (house.getOwner().isPresent()) {
                    final ItemStack owner = AvatarReturnsAPI.get().getUtils().getSkull(house.getOwner().get())
                            .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ANVIL));
                    AvatarReturnsAPI.get().getUtils().modifyItem(owner, "§cExpulser :");
                    lore.clear();
                    lore.add("  §7» §c" + Bukkit.getOfflinePlayer(house.getOwner().get()).getName());
                    AvatarReturnsAPI.get().getUtils().modifyItem(owner, lore);
                    inv.addItem(13, owner, true, inventoryClickEvent -> {
                        Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                        house.setOwner(null);
                    });
                }
                final ItemStack rentTime = AvatarReturnsAPI.get().getUtils().getItem(Material.CLOCK, "§cDurée du bail");
                lore.clear();
                lore.add("  §7» §c" + Utils.change(Math.ceil(house.getRentTime() * 100) / 100));
                AvatarReturnsAPI.get().getUtils().modifyItem(rentTime, lore);
                inv.addItem(14, rentTime, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Édition §8» §7Input")
                            .plugin(AvatarHouse.get())
                            .text(String.valueOf(house.getRentTime()))
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.CLOCK, String.valueOf(house.getRentTime())))
                            .onComplete((p, value) -> {
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                if (!isLong(value))
                                    return AnvilGUI.Response.text(String.valueOf(house.getRentTime()));
                                final long rentTimeValue = Long.parseLong(value);
                                if (rentTimeValue == house.getRentTime())
                                    return AnvilGUI.Response.text(String.valueOf(rentTimeValue));
                                p.performCommand("avatarhouse edit house \"" + house.getName() + "\" set rentTime " + rentTimeValue);
                                return AnvilGUI.Response.close();
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> editHouseMenu(house, p, page), 2))
                            .open(player);
                });
                final ItemStack members = AvatarReturnsAPI.get().getUtils().getItem(Material.PLAYER_HEAD, "§cMembres");
                inv.addItem(15, members, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    memberMenu(house, player);
                });
                inv.addItem(17, forward, true, inventoryClickEvent -> {
                    editHouseMenu(house, player, page + 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
            } else if (page == 2) {
                inv.addItem(9, backward, true, inventoryClickEvent -> {
                    editHouseMenu(house, player, page - 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                final ItemStack reset = AvatarReturnsAPI.get().getUtils().getItem(Material.TNT, "§cReset");
                lore.add("  §7» §c" + (house.isReset() ? "Oui" : "Non"));
                AvatarReturnsAPI.get().getUtils().modifyItem(reset, lore);
                inv.addItem(11, reset, true, inventoryClickEvent -> {
                    house.setReset(!house.isReset());
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    editHouseMenu(house, player, page);
                });
                final ItemStack coOwnerPrice = AvatarReturnsAPI.get().getUtils().getItem(Material.EMERALD, "§cPrix CoOwner");
                lore.clear();
                lore.add("  §7» §c" + Utils.change(Math.ceil(house.getCoOwnerPrice() * 100) / 100));
                AvatarReturnsAPI.get().getUtils().modifyItem(coOwnerPrice, lore);
                inv.addItem(12, coOwnerPrice, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Édition §8» §7Input")
                            .plugin(AvatarHouse.get())
                            .text(Utils.change(Math.ceil(house.getCoOwnerPrice() * 100) / 100))
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.EMERALD, String.valueOf(Utils.change(Math.ceil(house.getCoOwnerPrice() * 100) / 100))))
                            .onComplete((p, value) -> {
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                if (!isDouble(value))
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(house.getCoOwnerPrice() * 100) / 100));
                                final double coOwnerValue = Double.parseDouble(value);
                                if (coOwnerValue == house.getCoOwnerPrice())
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(coOwnerValue * 100) / 100));
                                p.performCommand("avatarhouse edit house \"" + house.getName() + "\" set coOwnerPrice " + coOwnerValue);
                                return AnvilGUI.Response.close();
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> editHouseMenu(house, p, page), 2))
                            .open(player);
                });
                final ItemStack memberPrice = AvatarReturnsAPI.get().getUtils().getItem(Material.IRON_INGOT, "§cPrix Membre");
                lore.clear();
                lore.add("  §7» §c" + Utils.change(Math.ceil(house.getMemberPrice() * 100) / 100));
                AvatarReturnsAPI.get().getUtils().modifyItem(memberPrice, lore);
                inv.addItem(13, memberPrice, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Édition §8» §7Input")
                            .plugin(AvatarHouse.get())
                            .text(Utils.change(Math.ceil(house.getMemberPrice() * 100) / 100))
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.IRON_INGOT, String.valueOf(Utils.change(Math.ceil(house.getMemberPrice() * 100) / 100))))
                            .onComplete((p, value) -> {
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                if (!isDouble(value))
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(house.getMemberPrice() * 100) / 100));
                                final double memberValue = Double.parseDouble(value);
                                if (memberValue == house.getMemberPrice())
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(memberValue * 100) / 100));
                                p.performCommand("avatarhouse edit house \"" + house.getName() + "\" set memberPrice " + memberValue);
                                return AnvilGUI.Response.close();
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> editHouseMenu(house, p, page), 2))
                            .open(player);
                });
                final ItemStack leavePrice = AvatarReturnsAPI.get().getUtils().getItem(Material.BREWING_STAND, "§cPrix quitter");
                lore.clear();
                lore.add("  §7» §c" + Utils.change(Math.ceil(house.getLeave() * 100) / 100));
                AvatarReturnsAPI.get().getUtils().modifyItem(leavePrice, lore);
                inv.addItem(14, leavePrice, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Édition §8» §7Input")
                            .plugin(AvatarHouse.get())
                            .text(Utils.change(Math.ceil(house.getLeave() * 100) / 100))
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.BREWING_STAND, String.valueOf(Utils.change(Math.ceil(house.getLeave() * 100) / 100))))
                            .onComplete((p, value) -> {
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                if (!isDouble(value))
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(house.getLeave() * 100) / 100));
                                final double leaveValue = Double.parseDouble(value);
                                if (leaveValue == house.getLeave())
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(leaveValue * 100) / 100));
                                p.performCommand("avatarhouse edit house \"" + house.getName() + "\" set leave " + leaveValue);
                                return AnvilGUI.Response.close();
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> editHouseMenu(house, p, page), 2))
                            .open(player);
                });
                final ItemStack strangerToMemberPrice = AvatarReturnsAPI.get().getUtils().getItem(Material.PLAYER_HEAD, "§cPrix ajouter Membre");
                lore.clear();
                lore.add("  §7» §c" + Utils.change(Math.ceil(house.getStrangerToMember() * 100) / 100));
                AvatarReturnsAPI.get().getUtils().modifyItem(strangerToMemberPrice, lore);
                inv.addItem(15, strangerToMemberPrice, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Édition §8» §7Input")
                            .plugin(AvatarHouse.get())
                            .text(Utils.change(Math.ceil(house.getStrangerToMember() * 100) / 100))
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.PLAYER_HEAD, String.valueOf(Utils.change(Math.ceil(house.getStrangerToMember() * 100) / 100))))
                            .onComplete((p, value) -> {
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                if (!isDouble(value))
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(house.getStrangerToMember() * 100) / 100));
                                final double strangerToMemberValue = Double.parseDouble(value);
                                if (strangerToMemberValue == house.getStrangerToMember())
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(strangerToMemberValue * 100) / 100));
                                p.performCommand("avatarhouse edit house \"" + house.getName() + "\" set strangerToMember " + strangerToMemberValue);
                                return AnvilGUI.Response.close();
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> editHouseMenu(house, p, page), 2))
                            .open(player);
                });
                inv.addItem(17, forward, true, inventoryClickEvent -> {
                    editHouseMenu(house, player, page + 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
            } else if (page == 3) {
                inv.addItem(9, backward, true, inventoryClickEvent -> {
                    editHouseMenu(house, player, page - 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                final ItemStack memberToCoOwnerPrice = AvatarReturnsAPI.get().getUtils().getItem(Material.DRAGON_HEAD, "§cPrix ajouter CoOwner");
                lore.clear();
                lore.add("  §7» §c" + Utils.change(Math.ceil(house.getMemberToCoOwner() * 100) / 100));
                AvatarReturnsAPI.get().getUtils().modifyItem(memberToCoOwnerPrice, lore);
                inv.addItem(11, memberToCoOwnerPrice, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Édition §8» §7Input")
                            .plugin(AvatarHouse.get())
                            .text(Utils.change(Math.ceil(house.getMemberToCoOwner() * 100) / 100))
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.DRAGON_HEAD, String.valueOf(Utils.change(Math.ceil(house.getMemberToCoOwner() * 100) / 100))))
                            .onComplete((p, value) -> {
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                if (!isDouble(value))
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(house.getMemberToCoOwner() * 100) / 100));
                                final double memberToCoOwnerValue = Double.parseDouble(value);
                                if (memberToCoOwnerValue == house.getMemberToCoOwner())
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(memberToCoOwnerValue * 100) / 100));
                                p.performCommand("avatarhouse edit house \"" + house.getName() + "\" set memberToCoOwner " + memberToCoOwnerValue);
                                return AnvilGUI.Response.close();
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> editHouseMenu(house, p, page), 2))
                            .open(player);
                });
                final ItemStack coOwnerToMemberPrice = AvatarReturnsAPI.get().getUtils().getItem(Material.REDSTONE, "§cPrix retirer CoOwner");
                lore.clear();
                lore.add("  §7» §c" + Utils.change(Math.ceil(house.getCoOwnerToMember() * 100) / 100));
                AvatarReturnsAPI.get().getUtils().modifyItem(coOwnerToMemberPrice, lore);
                inv.addItem(12, coOwnerToMemberPrice, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Édition §8» §7Input")
                            .plugin(AvatarHouse.get())
                            .text(Utils.change(Math.ceil(house.getCoOwnerToMember() * 100) / 100))
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.REDSTONE, String.valueOf(Utils.change(Math.ceil(house.getCoOwnerToMember() * 100) / 100))))
                            .onComplete((p, value) -> {
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                if (!isDouble(value))
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(house.getCoOwnerToMember() * 100) / 100));
                                final double coOwnerToMemberValue = Double.parseDouble(value);
                                if (coOwnerToMemberValue == house.getCoOwnerToMember())
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(coOwnerToMemberValue * 100) / 100));
                                p.performCommand("avatarhouse edit house \"" + house.getName() + "\" set coOwnerToMember " + coOwnerToMemberValue);
                                return AnvilGUI.Response.close();
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> editHouseMenu(house, p, page), 2))
                            .open(player);
                });
                final ItemStack memberToStrangerPrice = AvatarReturnsAPI.get().getUtils().getItem(Material.ARMOR_STAND, "§cPrix retirer Membre");
                lore.clear();
                lore.add("  §7» §c" + Utils.change(Math.ceil(house.getMemberToStranger() * 100) / 100));
                AvatarReturnsAPI.get().getUtils().modifyItem(memberToStrangerPrice, lore);
                inv.addItem(13, memberToStrangerPrice, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Édition §8» §7Input")
                            .plugin(AvatarHouse.get())
                            .text(Utils.change(Math.ceil(house.getMemberToStranger() * 100) / 100))
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.ARMOR_STAND, String.valueOf(Utils.change(Math.ceil(house.getMemberToStranger() * 100) / 100))))
                            .onComplete((p, value) -> {
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                if (!isDouble(value))
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(house.getMemberToStranger() * 100) / 100));
                                final double memberToStrangerValue = Double.parseDouble(value);
                                if (memberToStrangerValue == house.getMemberToStranger())
                                    return AnvilGUI.Response.text(Utils.change(Math.ceil(memberToStrangerValue * 100) / 100));
                                p.performCommand("avatarhouse edit house \"" + house.getName() + "\" set memberToStranger " + memberToStrangerValue);
                                return AnvilGUI.Response.close();
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> editHouseMenu(house, p, page), 2))
                            .open(player);
                });
                final ItemStack regions = AvatarReturnsAPI.get().getUtils().getItem(Material.TOTEM_OF_UNDYING, "§cRégions");
                inv.addItem(14, regions, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    regionManager(house, player, 1);
                });
                final ItemStack destroy = AvatarReturnsAPI.get().getUtils().getItem(Material.BEDROCK, "§4§lDÉTRUCTION");
                inv.addItem(15, destroy, true, inventoryClickEvent -> {
                    final InventoryAPI confirmInventory = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
                    confirmInventory.setTitle("§3§l" + house.getName() + " §8» §4§lDESTRUCTION").setSize(3 * 9).setRefresh(false);
                    final ItemStack houseItem = AvatarReturnsAPI.get().getUtils().getItem(Material.DIAMOND_BLOCK, "§6§l" + house.getName());
                    final List<String> lores = new ArrayList<>();
                    lores.add("");
                    lores.add("  §7» §9§lAction : §4§lDESTRUCTION");
                    lores.add("");
                    AvatarReturnsAPI.get().getUtils().modifyItem(houseItem, lores);
                    confirmInventory.addItem(22, backward, true, clickEvent -> {
                        editHouseMenu(house, player, page);
                        Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                    });
                    confirmInventory.addItem(13, houseItem, true);
                    for (int i = 0; i < 3 * 9; i++) {
                        if (i < 3 || i >= 9 && i < 12 || i >= 18 && i < 21)
                            confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_CONCRETE, "§a§lCONFIRMER"), true, clickEvent -> {
                                house.delete();
                                player.closeInventory();
                                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                            });
                        else if (i >= 6 && i < 9 || i >= 15 && i < 18 || i >= 24)
                            confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE, "§c§lANNULER"), true, clickEvent -> {
                                editHouseMenu(house, player, page);
                                Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                            });
                    }
                    confirmInventory.build(player);
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                });
            }
            inv.build(player);
        }

        private static void memberMenu(final House house, final Player player) {
            final InventoryAPI inv = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
            inv.setRefresh(false).setTitle("§3§l" + house.getName() + " §8» §7Membres");
            final ItemStack backward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjI1OTliZDk4NjY1OWI4Y2UyYzQ5ODg1MjVjOTRlMTlkZGQzOWZhZDA4YTM4Mjg0YTE5N2YxYjcwNjc1YWNjIn19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(backward, "§6§lRetour");
            final ItemStack add = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2VkZDIwYmU5MzUyMDk0OWU2Y2U3ODlkYzRmNDNlZmFlYjI4YzcxN2VlNmJmY2JiZTAyNzgwMTQyZjcxNiJ9fX0=")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARMOR_STAND));
            AvatarReturnsAPI.get().getUtils().modifyItem(add, "§6§lAjouter");
            if (house.getMembers().size() == 0 && house.getCo_owners().size() == 0) {
                inv.addItem(0, backward, true, inventoryClickEvent -> {
                    editHouseMenu(house, player, 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                inv.addItem(4, add, true, inventoryClickEvent -> {
                    addMember(house, player);
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                });
                inv.build(player);
                return;
            }
            inv.setRefresh(true).setSize(4 * 9);
            final Consumer<InventoryAPI> function = inventoryApi -> {
                for (int i = 0; i < 27; i++) {
                    inventoryApi.clearSlot(i);
                }
                inventoryApi.addItem(27, backward, true, inventoryClickEvent -> {
                    editHouseMenu(house, player, 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                int slot = 0;
                if (house.getOwner().isPresent()) {
                    final UUID owner = house.getOwner().get();
                    final ItemStack skull = AvatarReturnsAPI.get().getUtils().getSkull(owner).orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.PLAYER_HEAD));
                    AvatarReturnsAPI.get().getUtils().modifyItem(skull, "§4⚝ §c" + Bukkit.getOfflinePlayer(owner).getName() + " §4⚝");
                    inventoryApi.addItem(slot++, skull, true);
                }
                for (final UUID coowner : house.getCo_owners()) {
                    final ItemStack skull = AvatarReturnsAPI.get().getUtils().getSkull(coowner).orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.PLAYER_HEAD));
                    AvatarReturnsAPI.get().getUtils().modifyItem(skull, "§c" + Bukkit.getOfflinePlayer(coowner).getName() + " §4⚝");
                        final List<String> lore = new ArrayList<>();
                        lore.add("");
                        lore.add("§7» §6Clic gauche pour grader");
                        lore.add("§7» §6Clic droit pour rétrograder");
                        AvatarReturnsAPI.get().getUtils().modifyItem(skull, lore);
                    inventoryApi.addItem(slot++, skull, true, inventoryClickEvent -> {
                        if (inventoryClickEvent.isRightClick()) {
                            Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                            house.getCo_owners().remove(coowner);
                            house.getMembers().add(coowner);
                            house.updateRegions();
                            return;
                        } else if (inventoryClickEvent.isLeftClick()) {
                            Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                            house.getOwner().ifPresent(uuid -> house.getCo_owners().add(0, uuid));
                            house.getCo_owners().remove(coowner);
                            house.setOwner(coowner);
                            house.updateRegions();
                            return;
                        }
                        Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                    });
                }
                for (final UUID member : house.getMembers()) {
                    final ItemStack skull = AvatarReturnsAPI.get().getUtils().getSkull(member).orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.PLAYER_HEAD));
                    AvatarReturnsAPI.get().getUtils().modifyItem(skull, "§6" + Bukkit.getOfflinePlayer(member).getName());
                    final List<String> lore = new ArrayList<>();
                    lore.add("");
                        lore.add("§7» §6Clic gauche pour grader");

                    lore.add("§7» §6Clic droit pour supprimer");
                    AvatarReturnsAPI.get().getUtils().modifyItem(skull, lore);
                    inventoryApi.addItem(slot++, skull, true, inventoryClickEvent -> {
                        if (inventoryClickEvent.isRightClick()) {
                            Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                            house.getMembers().remove(member);
                            house.updateSign();
                            return;
                        } else if (inventoryClickEvent.isLeftClick()) {
                            Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                            house.getMembers().remove(member);
                            house.getCo_owners().add(member);
                            house.updateRegions();
                            return;
                        }
                        Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                    });
                }
                if (slot < Config.getHouse_max_members())
                    inv.addItem(slot, add, true, inventoryClickEvent -> {
                        addMember(house, player);
                        Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    });
            };
            inv.setFunction(function);
            inv.build(player);
        }

        private static void addMember(final House house, final Player player) {
            new AnvilGUI.Builder()
                    .title("§3§l" + house.getName() + " §8» §7Membres §8» §7Input")
                    .text("Joueur")
                    .item(AvatarReturnsAPI.get().getUtils().getItem(Material.PLAYER_HEAD, "Joueur"))
                    .plugin(AvatarHouse.get())
                    .onComplete((p, value) -> {
                        try {
                            final Optional<UUID> optionalUUID = AvatarReturnsAPI.get().getUtils().getFullUUID(AvatarReturnsAPI.get().getUtils().getUUID(value).get());
                            if (optionalUUID.isPresent()) {
                                house.getMembers().add(optionalUUID.get());
                                return AnvilGUI.Response.close();
                            }
                        } catch (Exception e) {
                        }
                        return AnvilGUI.Response.text("Joueur");
                    })
                    .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> memberMenu(house, p), 2))
                    .open(player);
        }

        private static void regionManager(final House house, final Player player, final int page) {
            final InventoryAPI inv = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
            inv.setRefresh(false).setTitle("§3§l" + house.getName() + " §8» §7Liste des régions §8» §7Page " + page);
            final ItemStack backward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjI1OTliZDk4NjY1OWI4Y2UyYzQ5ODg1MjVjOTRlMTlkZGQzOWZhZDA4YTM4Mjg0YTE5N2YxYjcwNjc1YWNjIn19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(backward, "§6§lRetour");
            final ItemStack add = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2VkZDIwYmU5MzUyMDk0OWU2Y2U3ODlkYzRmNDNlZmFlYjI4YzcxN2VlNmJmY2JiZTAyNzgwMTQyZjcxNiJ9fX0=")
                    .orElse(
                            AvatarReturnsAPI.get().getUtils().getItem(Material.ARMOR_STAND)
                    );
            AvatarReturnsAPI.get().getUtils().modifyItem(add, "§6Ajouter");
            if (house.getRegions().size() == 0 || 5 * 9 * (page - 1) > house.getRegions().size()) {
                inv.addItem(0, backward, true, inventoryClickEvent -> {
                    if (page == 1)
                        editHouseMenu(house, player, 1);
                    else
                        regionManager(house, player, page - 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                inv.addItem(4, add, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                    new AnvilGUI.Builder()
                            .title("§3§l" + house.getName() + " §8» §7Régions §8» §7Input")
                            .text("Région")
                            .item(AvatarReturnsAPI.get().getUtils().getItem(Material.TOTEM_OF_UNDYING, "Région"))
                            .plugin(AvatarHouse.get())
                            .onComplete((p, value) -> {
                                if (house.getRegions().contains(value)) {
                                    Utils.playSound(p, SoundEffects.ENTITY_VILLAGER_NO);
                                    return AnvilGUI.Response.text(value);
                                }
                                if (Utils.isRegion(house.getSign().get().convert().getWorld(), value)) {
                                    Utils.playSound(p, SoundEffects.UI_BUTTON_CLICK);
                                    p.performCommand("houses edit house \"" + house.getName() + "\" regions add \"" + value + "\"");
                                    return AnvilGUI.Response.close();
                                }
                                Utils.playSound(p, SoundEffects.ENTITY_VILLAGER_NO);
                                return AnvilGUI.Response.text(value);
                            })
                            .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> regionManager(house, p, page), 2))
                            .open(player);
                });
                inv.build(player);
                return;
            }
            inv.setSize(6 * 9);
            inv.addItem(45, backward, true, inventoryClickEvent -> {
                if (page == 1)
                    editHouseMenu(house, player, 3);
                else
                    regionManager(house, player, page - 1);
                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
            });
            inv.addItem(49, add, true, inventoryClickEvent -> {
                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                new AnvilGUI.Builder()
                        .title("§3§l" + house.getName() + " §8» §7Régions §8» §7Input")
                        .text("Région")
                        .item(AvatarReturnsAPI.get().getUtils().getItem(Material.TOTEM_OF_UNDYING, "Région"))
                        .plugin(AvatarHouse.get())
                        .onComplete((p, value) -> {
                            if (house.getRegions().contains(value)) {
                                Utils.playSound(p, SoundEffects.ENTITY_VILLAGER_NO);
                                return AnvilGUI.Response.text(value);
                            }
                            if (Utils.isRegion(house.getSign().get().convert().getWorld(), value)) {
                                Utils.playSound(p, SoundEffects.UI_BUTTON_CLICK);
                                p.performCommand("houses edit house \"" + house.getName() + "\" regions add \"" + value + "\"");
                                return AnvilGUI.Response.close();
                            }
                            Utils.playSound(p, SoundEffects.ENTITY_VILLAGER_NO);
                            return AnvilGUI.Response.text(value);
                        })
                        .onClose(p -> Bukkit.getScheduler().runTaskLater(AvatarHouse.get(), () -> regionManager(house, p, page), 2))
                        .open(player);
            });
            final ItemStack forward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzJmOTEwYzQ3ZGEwNDJlNGFhMjhhZjZjYzgxY2Y0OGFjNmNhZjM3ZGFiMzVmODhkYjk5M2FjY2I5ZGZlNTE2In19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(forward, "§6Page suivante");
            int slot = 0;
            int index = 5 * 9 * (page - 1);
            final List<String> lore = new ArrayList<>();
            lore.add("");
            lore.add("§7» §6Cliquez pour supprimer");
            while (index < Math.min(5 * 9 * page, house.getRegions().size())) {
                final ItemStack region = AvatarReturnsAPI.get().getUtils().getItem(Material.TOTEM_OF_UNDYING, "§c" + house.getRegions().get(index));
                AvatarReturnsAPI.get().getUtils().modifyItem(region, lore);
                int finalIndex = index;
                inv.addItem(slot++, region, true, inventoryClickEvent -> {
                    player.performCommand("houses edit house \"" + house.getName() + "\" regions remove \"" + house.getRegions().get(finalIndex) + "\"");
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    regionManager(house, player, page);
                });
                index++;
            }
            if (index < house.getRegions().size()) {
                inv.addItem(53, forward, true, inventoryClickEvent -> {
                    regionManager(house, player, page + 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
            }
            inv.build(player);
        }

        private static boolean isDouble(final String value) {
            try {
                Double.parseDouble(value);
                return true;
            } catch (Exception ignored) {
            }
            return false;
        }

        private static boolean isLong(final String value) {
            try {
                Long.parseLong(value);
                return true;
            } catch (Exception ignored) {
            }
            return false;
        }

    }

}
