package fr.avatarreturns.avatarhouse.system;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.inventory.InventoryAPI;
import fr.avatarreturns.api.users.IUser;
import fr.avatarreturns.avatarhouse.configuration.Config;
import fr.avatarreturns.avatarhouse.configuration.GsonSerializer;
import fr.avatarreturns.avatarhouse.messages.Messages;
import fr.avatarreturns.avatarhouse.plugin.AvatarHouse;
import fr.avatarreturns.avatarhouse.utils.Utils;
import net.minecraft.server.v1_16_R3.SoundEffects;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Evoker;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.loot.LootTables;
import org.bukkit.metadata.FixedMetadataValue;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Neighbourhood {

    private transient static final List<Neighbourhood> neighbourhoods = new ArrayList<>();
    private final UUID uniqueId;
    private final List<UUID> houses;
    @Parameter("setName")
    private String name;
    @Parameter("setMaxHouses")
    private int maxHouses;
    private Location realtor;
    private transient Evoker entity;
    private UUID entityId;

    public Neighbourhood(final String name) {
        this.uniqueId = UUID.randomUUID();
        this.name = name;
        this.houses = new ArrayList<>();
        this.maxHouses = Config.getNeighbourhood_max_houses();
        this.realtor = null;
        this.entity = null;
        this.entityId = null;
    }

    public static List<Neighbourhood> getNeighbourhoods() {
        return neighbourhoods;
    }

    public static Optional<Neighbourhood> getNeighbourhood(final UUID uniqueId) {
        return neighbourhoods.stream().filter(neighbourhood -> neighbourhood.getUniqueId().equals(uniqueId)).findFirst();
    }

    public static Optional<Neighbourhood> getNeighbourhood(final String name) {
        return neighbourhoods.stream().filter(neighbourhood -> neighbourhood.getName().equals(name)).findFirst();
    }

    public static void save(final Neighbourhood neighbourhood) {
        AvatarReturnsAPI.get().debug("Saving neighbourhood : " + neighbourhood.getUniqueId() + " ...", AvatarHouse.get());
        final File file = new File(AvatarHouse.get().getNeighborhoodSavePath(), neighbourhood.getUniqueId() + ".json");
        try {
            if (!file.exists()) {
                AvatarReturnsAPI.get().debug("... creating file ...", AvatarHouse.get());
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            try (FileWriter output = new FileWriter(file)) {
                AvatarReturnsAPI.get().debug("... writing file ...", AvatarHouse.get());
                output.write(GsonSerializer.get().getGson().toJson(neighbourhood));
            } catch (IOException e) {
                AvatarReturnsAPI.get().debug("... FAILED.", AvatarHouse.get());
                e.printStackTrace();
                return;
            }
        } catch (IOException e) {
            AvatarReturnsAPI.get().debug("... FAILED.", AvatarHouse.get());
            e.printStackTrace();
            return;
        }
        AvatarReturnsAPI.get().debug("... SUCCESS.", AvatarHouse.get());
    }

    public static Optional<Neighbourhood> load(final File file) {
        AvatarReturnsAPI.get().debug("Loading neighbourhood " + file.getName() + "...", AvatarHouse.get());
        if (!file.exists()) {
            AvatarReturnsAPI.get().debug("... not found ...", AvatarHouse.get());
            AvatarReturnsAPI.get().debug("... FAILED.", AvatarHouse.get());
            return Optional.empty();
        }
        try (final FileReader input = new FileReader(file)) {
            AvatarReturnsAPI.get().debug("... reading file ...", AvatarHouse.get());
            final Neighbourhood neighbourhood = GsonSerializer.get().getGson().fromJson(input, Neighbourhood.class);
            if (neighbourhood == null) {
                AvatarReturnsAPI.get().debug("... FAILED.", AvatarHouse.get());
                return Optional.empty();
            }
            AvatarReturnsAPI.get().debug("... SUCCESS.", AvatarHouse.get());
            return Optional.of(neighbourhood);
        } catch (IOException e) {
            AvatarReturnsAPI.get().debug("... FAILED.", AvatarHouse.get());
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public static List<Optional<Neighbourhood>> load(final String directory) {
        final File file = new File(directory);
        if (!file.exists())
            return new ArrayList<>();
        return Arrays.stream(file.listFiles()).map(Neighbourhood::load).collect(Collectors.toList());
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
        this.save();
    }

    public List<UUID> getHouses() {
        return this.houses;
    }

    public int getMaxHouses() {
        return maxHouses;
    }

    public void setMaxHouses(final int maxHouses) {
        this.maxHouses = maxHouses;
    }

    public Optional<Location> getRealtor() {
        return Optional.ofNullable(this.realtor);
    }

    public void setRealtor(final @Nullable Location realtor) {
        this.realtor = realtor;
        this.save();
    }

    public void generateEntity() {
        if (this.realtor == null)
            return;
        if (this.entity == null && this.entityId != null)
            this.realtor.convert().getWorld().getNearbyEntities(this.realtor.convert(), 2, 2, 2)
                    .stream().filter(entities -> entities.getUniqueId().equals(this.entityId))
                    .forEach(Entity::remove);
        this.entity = this.realtor.convert().getWorld().spawn(this.realtor.convert(), Evoker.class);
        this.entityId = this.entity.getUniqueId();
        this.entity.setAI(false);
        this.entity.setAware(false);
        this.entity.setCanJoinRaid(false);
        this.entity.addAttachment(AvatarHouse.get());
        this.entity.setPatrolLeader(false);
        this.entity.setCollidable(false);
        this.entity.setRemoveWhenFarAway(false);
        this.entity.setInvulnerable(true);
        this.entity.setGravity(true);
        this.entity.setSilent(true);
        this.entity.setLootTable(LootTables.EMPTY.getLootTable());
        this.entity.setCustomNameVisible(true);
        this.entity.setCustomName(Config.getNeighbourhood_default_name());
        this.entity.getEquipment().clear();
        this.entity.setMetadata("NeighbourhoodId", new FixedMetadataValue(AvatarHouse.get(), this.getUniqueId().toString()));
    }

    public void removeEntity() {
        if (this.entity == null)
            return;
        if (this.entity.isDead()) {
            this.entity = null;
            return;
        }
        this.entity.remove();
        this.entity = null;
    }

    public boolean isOwner(final UUID uniqueId) {
        return this.getHouses().stream().anyMatch(uuid -> House.getHouse(uuid).isPresent() && House.getHouse(uuid).get().isOwner(uniqueId));
    }

    public boolean isCoOwner(final UUID uniqueId) {
        return this.getHouses().stream().anyMatch(uuid -> House.getHouse(uuid).isPresent() && House.getHouse(uuid).get().isCoOwner(uniqueId));
    }

    public boolean isMember(final UUID uniqueId) {
        return this.getHouses().stream().anyMatch(uuid -> House.getHouse(uuid).isPresent() && House.getHouse(uuid).get().isMember(uniqueId));
    }

    public void save() {
        save(this);
    }

    public void delete() {
        getHouses().remove(this);
        try {
            final File file = new File(AvatarHouse.get().getNeighborhoodSavePath(), this.getUniqueId() + ".json");
            if (file.exists())
                file.delete();
            this.finalize();
        } catch (Throwable ignored) {
        }
    }

    public static class Inventories {


        public static void editRealtor(final Neighbourhood neighbourhood, final Player player, final int page) {
            final InventoryAPI inv = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
            inv.setRefresh(false).setTitle("§3§l" + neighbourhood.getName() + " §8» §7Liste des maisons §8» §7Page " + page);
            final ItemStack backward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjI1OTliZDk4NjY1OWI4Y2UyYzQ5ODg1MjVjOTRlMTlkZGQzOWZhZDA4YTM4Mjg0YTE5N2YxYjcwNjc1YWNjIn19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(backward, "§6§lRetour");
            final ItemStack add = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2VkZDIwYmU5MzUyMDk0OWU2Y2U3ODlkYzRmNDNlZmFlYjI4YzcxN2VlNmJmY2JiZTAyNzgwMTQyZjcxNiJ9fX0=")
                    .orElse(
                            AvatarReturnsAPI.get().getUtils().getItem(Material.ARMOR_STAND)
                    );
            AvatarReturnsAPI.get().getUtils().modifyItem(add, "§6Ajouter");
            if (neighbourhood.getHouses().size() == 0 || 5 * 9 * (page - 1) > neighbourhood.getHouses().size()) {
                inv.addItem(0, backward, true, inventoryClickEvent -> {
                    if (page == 1)
                        player.closeInventory();
                    else
                        editRealtor(neighbourhood, player, page - 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                inv.addItem(4, add, true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                    addHouseMenu(neighbourhood, player, 1);
                });
                inv.build(player);
                return;
            }
            inv.setSize(6 * 9);
            inv.addItem(45, backward, true, inventoryClickEvent -> {
                if (page == 1)
                    player.closeInventory();
                else
                    editRealtor(neighbourhood, player, page - 1);
                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
            });
            inv.addItem(49, add, true, inventoryClickEvent -> {
                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                addHouseMenu(neighbourhood, player, 1);
            });
            final ItemStack forward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzJmOTEwYzQ3ZGEwNDJlNGFhMjhhZjZjYzgxY2Y0OGFjNmNhZjM3ZGFiMzVmODhkYjk5M2FjY2I5ZGZlNTE2In19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(forward, "§6Page suivante");
            int slot = 0;
            int index = 5 * 9 * (page - 1);
            final List<String> lore = new ArrayList<>();
            lore.add("");
            lore.add("§7» §6Clic gauche pour éditer");
            lore.add("§7» §6Clic droit pour retirer");
            while (index < Math.min(5 * 9 * page, neighbourhood.getHouses().size())) {
                final Optional<House> house = House.getHouse(neighbourhood.getHouses().get(index));
                if (!house.isPresent()) {
                    index++;
                    continue;
                }
                final ItemStack item = AvatarReturnsAPI.get().getUtils().getItem(Material.CHEST, "§c" + house.get().getName());
                AvatarReturnsAPI.get().getUtils().modifyItem(item, lore);
                inv.addItem(slot++, item, true, inventoryClickEvent -> {
                    if (inventoryClickEvent.isLeftClick()) {
                        Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                        House.Inventories.editHouseMenu(house.get(), player, 1);
                    } else if (inventoryClickEvent.isRightClick()) {
                        player.performCommand("houses edit neighbourhood \"" + neighbourhood.getName() + "\" houses remove \"" + house.get().getName() + "\"");
                        Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                        editRealtor(neighbourhood, player, page);
                    }
                });
                index++;
            }
            if (index < neighbourhood.getHouses().size()) {
                inv.addItem(53, forward, true, inventoryClickEvent -> {
                    editRealtor(neighbourhood, player, page + 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
            }
            inv.build(player);
        }

        private static void addHouseMenu(final Neighbourhood neighbourhood, final Player player, final int page) {
            final InventoryAPI inv = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
            inv.setRefresh(false).setTitle("§3§l" + neighbourhood.getName() + " §8» §7Ajouter des maisons §8» §7Page " + page);
            final ItemStack backward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjI1OTliZDk4NjY1OWI4Y2UyYzQ5ODg1MjVjOTRlMTlkZGQzOWZhZDA4YTM4Mjg0YTE5N2YxYjcwNjc1YWNjIn19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(backward, "§6§lRetour");
            final List<House> houses = House.getHouses().stream()
                    .filter(house -> !house.isLink())
                    .collect(Collectors.toList());
            if (houses.size() == 0 || 5 * 9 * (page - 1) > houses.size()) {
                inv.addItem(0, backward, true, inventoryClickEvent -> {
                    if (page == 1)
                        editRealtor(neighbourhood, player, 1);
                    else
                        addHouseMenu(neighbourhood, player, page - 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                inv.build(player);
                return;
            }
            inv.setSize(6 * 9);
            inv.addItem(45, backward, true, inventoryClickEvent -> {
                if (page == 1)
                    editRealtor(neighbourhood, player, 1);
                else
                    addHouseMenu(neighbourhood, player, page - 1);
                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
            });
            final ItemStack forward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzJmOTEwYzQ3ZGEwNDJlNGFhMjhhZjZjYzgxY2Y0OGFjNmNhZjM3ZGFiMzVmODhkYjk5M2FjY2I5ZGZlNTE2In19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(forward, "§6Page suivante");
            int slot = 0;
            int index = 5 * 9 * (page - 1);
            final List<String> lore = new ArrayList<>();
            lore.add("");
            lore.add("§7» §6Cliquez ajouter");
            while (index < Math.min(5 * 9 * page, houses.size())) {
                final House house = houses.get(index);
                final ItemStack item = AvatarReturnsAPI.get().getUtils().getItem(Material.CHEST, "§c" + house.getName());
                AvatarReturnsAPI.get().getUtils().modifyItem(item, lore);
                inv.addItem(slot++, item, true, inventoryClickEvent -> {
                    player.performCommand("houses edit neighbourhood \"" + neighbourhood.getName() + "\" houses add \"" + house.getName() + "\"");
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    addHouseMenu(neighbourhood, player, page);
                });
                index++;
            }
            if (index < houses.size()) {
                inv.addItem(53, forward, true, inventoryClickEvent -> {
                    addHouseMenu(neighbourhood, player, page + 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
            }
            inv.build(player);
        }

        public static void realtorMenu(final Neighbourhood neighbourhood, final Player player) {
            final InventoryAPI inv = AvatarReturnsAPI.get().getInventory(AvatarHouse.get())
                    .setRefresh(false)
                    .setSize(9)
                    .setTitle("§3§l" + neighbourhood.getName());
            if (neighbourhood.isOwner(player.getUniqueId()) || neighbourhood.isCoOwner(player.getUniqueId()) || neighbourhood.isMember(player.getUniqueId())) {
                inv.addItem(1, AvatarReturnsAPI.get().getUtils().getItem(Material.COMMAND_BLOCK, "§6§lGérer mes locations"), true, inventoryClickEvent -> {
                    listLocations(neighbourhood, player);
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                });
                inv.addItem(7, AvatarReturnsAPI.get().getUtils().getItem(Material.COMMAND_BLOCK, "§6§lGérer mes locations"), true, inventoryClickEvent -> {
                    listLocations(neighbourhood, player);
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                });
            }
            inv.addItem(4, AvatarReturnsAPI.get().getUtils().getItem(Material.OAK_SIGN, "§c§lListe des maisons"), true, inventoryClickEvent -> {
                listHouses(neighbourhood, player, 1);
                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
            });
            inv.build(player);
        }

        public static void listHouses(final Neighbourhood neighbourhood, final Player player, int page) {
            final InventoryAPI inv = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
            inv.setRefresh(false).setTitle("§3§l" + neighbourhood.getName() + " §8» §7Liste des maisons §8» §7Page " + page);
            final ItemStack backward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjI1OTliZDk4NjY1OWI4Y2UyYzQ5ODg1MjVjOTRlMTlkZGQzOWZhZDA4YTM4Mjg0YTE5N2YxYjcwNjc1YWNjIn19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(backward, "§6§lRetour");
            if (neighbourhood.getHouses().stream().map(uuid -> House.getHouse(uuid).get()).allMatch(House::isInvalid)) {
                inv.addItem(0, backward, true, inventoryClickEvent -> {
                    if (page == 1)
                        realtorMenu(neighbourhood, player);
                    else
                        listHouses(neighbourhood, player, page - 1);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                inv.addItem(4, AvatarReturnsAPI.get().getUtils().getItem(Material.STRUCTURE_VOID, "§cIl n'y a aucune maison"), true);
                inv.build(player);
                return;
            }
            inv.setSize(6 * 9);
            inv.addItem(45, backward, true, inventoryClickEvent -> {
                if (page == 1)
                    realtorMenu(neighbourhood, player);
                else
                    listHouses(neighbourhood, player, page - 1);
                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
            });
            final ItemStack forward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzJmOTEwYzQ3ZGEwNDJlNGFhMjhhZjZjYzgxY2Y0OGFjNmNhZjM3ZGFiMzVmODhkYjk5M2FjY2I5ZGZlNTE2In19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            inv.build(player);
            final int playersHouses = (int) neighbourhood.getHouses().stream().filter(uuid -> House.getHouse(uuid).isPresent() && House.getHouse(uuid).get().isOwner(player.getUniqueId())).count();
            new Thread(() -> {
                int slot = 0;
                int index = 5 * 9 * (page - 1);
                while (index < Math.min(5 * 9 * page, neighbourhood.getHouses().size())) {
                    final Optional<House> house = House.getHouse(neighbourhood.getHouses().get(index));
                    if (house.isPresent()) {
                        if (house.get().isInvalid())
                            slot--;
                        if (house.get().getState().equals(House.State.FREE)) {
                            final ItemStack item = AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_TERRACOTTA, "§a" + house.get().getName());
                            final List<String> lore = new ArrayList<>();
                            lore.add("");
                            lore.add("  §7» §6§lPrix : §e" + Utils.change(Math.ceil(house.get().getPrice() * 100) / 100));
                            lore.add("  §7» §6§lLoyer : §e" + Utils.change(Math.ceil(house.get().getRent() * 100) / 100));
                            final int distance = (int) player.getLocation().distance(house.get().getSign().get().convert());
                            lore.add("  §7» §6§lDistance : §e" + distance + " bloc(s) " + Utils.getDirection(player.getLocation(), house.get().getSign().get().convert()));
                            lore.add("");
                            if (playersHouses <= neighbourhood.getMaxHouses()) {
                                lore.add("§8§l» §eCliquez pour acheter");
                                lore.add("");
                            }
                            AvatarReturnsAPI.get().getUtils().modifyItem(item, lore);
                            inv.addItem(slot, item, true, inventoryClickEvent -> {
                                if (playersHouses > neighbourhood.getMaxHouses()) {
                                    Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                    return;
                                }
                                final Optional<IUser> user = AvatarReturnsAPI.get().getUser(player);
                                if (!user.isPresent()) {
                                    Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                    return;
                                }
                                final InventoryAPI confirmInventory = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
                                confirmInventory.setTitle("§3§l" + neighbourhood.getName() + " §8» §7Achat").setSize(3 * 9).setRefresh(false);
                                final ItemStack houseItem = AvatarReturnsAPI.get().getUtils().getItem(Material.DIAMOND_BLOCK, "§6§l" + house.get().getName());
                                final List<String> lores = new ArrayList<>();
                                lores.add("");
                                lores.add("  §7» §9§lAction : §3Achat");
                                lores.add("  §7» §9§lPrix : §3" + Utils.change(Math.ceil(house.get().getPrice() * 100) / 100));
                                lores.add("  §7» §9§lLoyer : §3" + Utils.change(Math.ceil(house.get().getRent() * 100) / 100));
                                lores.add("  §7» §9§lDistance : §3" + distance + " bloc(s) " + Utils.getDirection(player.getLocation(), house.get().getSign().get().convert()));
                                lores.add("");
                                AvatarReturnsAPI.get().getUtils().modifyItem(houseItem, lores);
                                confirmInventory.addItem(22, backward, true, clickEvent -> {
                                    listHouses(neighbourhood, player, page);
                                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                                });
                                confirmInventory.addItem(13, houseItem, true);
                                for (int i = 0; i < 3 * 9; i++) {
                                    if (i < 3 || i >= 9 && i < 12 || i >= 18 && i < 21)
                                        confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_CONCRETE, "§a§lCONFIRMER"), true, clickEvent -> {
                                            if (user.get().getMoney() >= house.get().getPrice()) {
                                                if (house.get().getState().equals(House.State.FREE)) {
                                                    house.get().setOwner(player.getUniqueId());
                                                    house.get().setLastRentTime(System.currentTimeMillis());
                                                    user.get().removeMoney(house.get().getPrice());
                                                    Utils.playSound(player, SoundEffects.ITEM_FLINTANDSTEEL_USE);
                                                } else {
                                                    Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                                }
                                            } else {
                                                Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                                player.sendMessage(Messages.ERROR_MONEY.get());
                                            }
                                            player.closeInventory();
                                        });
                                    else if (i >= 6 && i < 9 || i >= 15 && i < 18 || i >= 24)
                                        confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE, "§c§lANNULER"), true, clickEvent -> {
                                            player.closeInventory();
                                            Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                        });
                                }
                                confirmInventory.build(player);
                                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                            });
                        } else if (house.get().getState().equals(House.State.OCCUPIED)) {
                            final ItemStack item = AvatarReturnsAPI.get().getUtils().getItem(Material.RED_TERRACOTTA, "§c" + house.get().getName());
                            final List<String> lore = new ArrayList<>();
                            lore.add("");
                            lore.add("  §7» §4§lPropriétaire : §c" + Bukkit.getOfflinePlayer(house.get().getOwner().get()).getName());
                            final int distance = (int) player.getLocation().distance(house.get().getSign().get().convert());
                            lore.add("  §7» §4§lDistance : §c" + distance + " bloc(s) " + Utils.getDirection(player.getLocation(), house.get().getSign().get().convert()));
                            lore.add("");
                            AvatarReturnsAPI.get().getUtils().modifyItem(item, lore);
                            inv.addItem(slot, item, true, inventoryClickEvent -> {
                                if (house.get().isOwner(player.getUniqueId()) || house.get().isCoOwner(player.getUniqueId())) {
                                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                                    editHouse(neighbourhood, player, house.get());
                                } else {
                                    Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                }
                            });
                        }
                        slot++;
                    }
                    index++;
                }
                if (index < neighbourhood.getHouses().size())
                    inv.addItem(53, forward, true, object -> {
                        listHouses(neighbourhood, player, page + 1);
                        Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                    });
                inv.build(player);
            }).start();
        }

        public static void listLocations(final Neighbourhood neighbourhood, final Player player) {
            final InventoryAPI inv = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
            inv.setRefresh(false).setTitle("§3§l" + neighbourhood.getName() + " §8» §7Liste des locations");
            final ItemStack backward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjI1OTliZDk4NjY1OWI4Y2UyYzQ5ODg1MjVjOTRlMTlkZGQzOWZhZDA4YTM4Mjg0YTE5N2YxYjcwNjc1YWNjIn19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(backward, "§6§lRetour");
            final List<House> playersHouses = neighbourhood.getHouses().stream().map(uuid -> House.getHouse(uuid).get())
                    .filter(house -> house.isOwner(player.getUniqueId()) || house.isCoOwner(player.getUniqueId()) || house.isMember(player.getUniqueId()))
                    .collect(Collectors.toList());
            if (playersHouses.stream().allMatch(House::isInvalid)) {
                inv.addItem(0, backward, true, inventoryClickEvent -> {
                    realtorMenu(neighbourhood, player);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                inv.addItem(4, AvatarReturnsAPI.get().getUtils().getItem(Material.STRUCTURE_VOID, "§cIl n'y a aucune maison"), true, inventoryClickEvent ->
                        Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO));
                inv.build(player);
                return;
            }
            inv.setSize(6 * 9);
            inv.addItem(45, backward, true, inventoryClickEvent -> {
                realtorMenu(neighbourhood, player);
                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
            });
            inv.build(player);
            new Thread(() -> {
                int slot = 0;
                int index = 0;
                while (index < Math.min(5 * 9, playersHouses.size())) {
                    final House house = playersHouses.get(index);
                    if (house.isInvalid()) {
                        index++;
                        continue;
                    }
                    final ItemStack item = AvatarReturnsAPI.get().getUtils().getItem(Material.CHEST, "§3§l" + house.getName());
                    final List<String> lore = new ArrayList<>();
                    lore.add("");
                    if (!house.isMember(player.getUniqueId()))
                        lore.add("  §7» §9§lStatut : §3" + (house.isOwner(player.getUniqueId()) ? "Propriétaire" : "Co-Propriétaire"));
                    else {
                        lore.add("  §7» §9§lStatut : §3Membre");
                        lore.add("");
                        lore.add("§7» §6§lCliquez pour quitter");
                    }
                    lore.add("");
                    AvatarReturnsAPI.get().getUtils().modifyItem(item, lore);
                    inv.addItem(slot, item, true, inventoryClickEvent -> {
                        Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                        if (house.isMember(player.getUniqueId())) {
                            final InventoryAPI confirmInventory = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
                            confirmInventory.setTitle("§3§l" + house.getName() + " §8» §7Confirmation").setSize(3 * 9).setRefresh(false);
                            final List<String> lores = new ArrayList<>();
                            lores.add("");
                            lores.add("  §7» §9§lAction : §3Quitter");
                            lores.add("  §7» §9§lPrix : §3" + Utils.change(house.getLeave()));
                            lores.add("");
                            AvatarReturnsAPI.get().getUtils().modifyItem(item, lores);
                            confirmInventory.addItem(22, backward, true, clickEvent -> {
                                listLocations(neighbourhood, player);
                                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                            });
                            confirmInventory.addItem(13, item, true);
                            for (int i = 0; i < 3 * 9; i++) {
                                if (i < 3 || i >= 9 && i < 12 || i >= 18 && i < 21)
                                    confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_CONCRETE, "§a§lCONFIRMER"), true, clickEvent -> {
                                        final Optional<IUser> userOptional = AvatarReturnsAPI.get().getUser(player);
                                        if (userOptional.isPresent()) {
                                            if (userOptional.get().getMoney() >= house.getLeave()) {
                                                if (house.isMember(player.getUniqueId()) || house.isCoOwner(player.getUniqueId())) {
                                                    house.getCo_owners().remove(player.getUniqueId());
                                                    house.getMembers().remove(player.getUniqueId());
                                                    house.updateRegions();
                                                    userOptional.get().removeMoney(house.getLeave());
                                                    Utils.playSound(player, SoundEffects.ITEM_FLINTANDSTEEL_USE);
                                                } else
                                                    Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                            } else {
                                                Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                                player.sendMessage(Messages.ERROR_MONEY.get());
                                            }
                                        } else
                                            Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                        realtorMenu(neighbourhood, player);
                                    });
                                else if (i >= 6 && i < 9 || i >= 15 && i < 18 || i >= 24)
                                    confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE, "§c§lANNULER"), true, clickEvent -> {
                                        Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                        listLocations(neighbourhood, player);
                                    });
                            }
                            confirmInventory.build(player);
                        } else
                            editHouse(neighbourhood, player, house);
                    });
                    slot++;
                    index++;
                }
                inv.build(player);
            }).start();
        }

        public static void editHouse(final Neighbourhood neighbourhood, final Player player, final House house) {
            final InventoryAPI inv = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
            inv.setRefresh(true).setTitle("§3§l" + house.getName() + " §8» §7Gestion").setSize(3 * 9);
            final ItemStack backward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjI1OTliZDk4NjY1OWI4Y2UyYzQ5ODg1MjVjOTRlMTlkZGQzOWZhZDA4YTM4Mjg0YTE5N2YxYjcwNjc1YWNjIn19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(backward, "§6§lRetour");
            inv.addItem(0, backward, true, inventoryClickEvent -> {
                listLocations(neighbourhood, player);
                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
            });
            inv.addItem(11, AvatarReturnsAPI.get().getUtils().modifyItem(
                    AvatarReturnsAPI.get().getUtils().getSkull(house.getOwner().get()).orElseGet(() ->
                            AvatarReturnsAPI.get().getUtils().getItem(Material.BRICKS)),
                    "§cGérer les membres"), true, inventoryClickEvent -> {
                editMember(neighbourhood, player, house);
                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
            });
            final ItemStack houseItem = AvatarReturnsAPI.get().getUtils().getItem(Material.DIAMOND_BLOCK, "§6§l" + house.getName());
            if (house.isOwner(player.getUniqueId())) {
                inv.addItem(13, AvatarReturnsAPI.get().getUtils().getItem(Material.GOLD_INGOT, "§4Arrêter la location"), true, inventoryClickEvent -> {
                    final InventoryAPI confirmInventory = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
                    confirmInventory.setTitle("§3§l" + house.getName() + " §8» §7Arrêt de la location").setSize(3 * 9).setRefresh(false);
                    final List<String> lores = new ArrayList<>();
                    lores.add("");
                    lores.add("  §7» §9§lPrix : §3" + Utils.change(Math.ceil(house.getPrice() * 100) / 100));
                    final double defaultLoyer = house.getRent();
                    final double coOwner = house.getCo_owners().size() * house.getCoOwnerPrice();
                    final double member = house.getMembers().size() * house.getMemberPrice();
                    final double toPay = Math.ceil((defaultLoyer + coOwner + member) * 100) / 100;
                    lores.add("  §7» §9§lTotal : §3" + Utils.change(toPay));
                    lores.add("      §7» §9Base : §3" + Utils.change(Math.ceil(defaultLoyer * 100) / 100));
                    if (house.getCo_owners().size() > 0)
                        lores.add("      §7» §9Co-proprio : §3" + house.getCo_owners().size() + " * " + Utils.change(Math.ceil(house.getCoOwnerPrice() * 100) / 100));
                    if (house.getMembers().size() > 0)
                        lores.add("      §7» §9Membres : §3" + house.getMembers().size() + " * " + Utils.change(Math.ceil(house.getMemberPrice() * 100) / 100));
                    lores.add("");
                    final int distance = (int) player.getLocation().distance(house.getSign().get().convert());
                    lores.add("  §7» §9§lDistance : §3" + distance + " bloc(s) " + Utils.getDirection(player.getLocation(), house.getSign().get().convert()));
                    lores.add("");
                    AvatarReturnsAPI.get().getUtils().modifyItem(houseItem, lores);
                    confirmInventory.addItem(22, backward, true, clickEvent -> {
                        editHouse(neighbourhood, player, house);
                        Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                    });
                    confirmInventory.addItem(13, houseItem, true);
                    for (int i = 0; i < 3 * 9; i++) {
                        if (i < 3 || i >= 9 && i < 12 || i >= 18 && i < 21)
                            confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_CONCRETE, "§a§lCONFIRMER"), true, clickEvent -> {
                                if (house.isOwner(player.getUniqueId())) {
                                    house.setOwner(null);
                                }
                                Utils.playSound(player, SoundEffects.ITEM_FLINTANDSTEEL_USE);
                                player.closeInventory();
                            });
                        else if (i >= 6 && i < 9 || i >= 15 && i < 18 || i >= 24)
                            confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE, "§c§lANNULER"), true, clickEvent -> {
                                Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                player.closeInventory();
                            });
                    }
                    confirmInventory.build(player);
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                });
            } else {
                inv.addItem(13, AvatarReturnsAPI.get().getUtils().modifyItem(AvatarReturnsAPI.get().getUtils().getItem(Material.BARRIER), "§cQuitter la location"), true, inventoryClickEvent -> {
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    final InventoryAPI confirmInventory = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
                    confirmInventory.setTitle("§3§l" + house.getName() + " §8» §7Confirmation").setSize(3 * 9).setRefresh(false);
                    final List<String> lores = new ArrayList<>();
                    lores.add("");
                    lores.add("  §7» §9§lAction : §3Quitter");
                    lores.add("  §7» §9§lPrix : §3" + Utils.change(house.getLeave()));
                    lores.add("");
                    AvatarReturnsAPI.get().getUtils().modifyItem(houseItem, lores);
                    confirmInventory.addItem(22, backward, true, clickEvent -> {
                        listLocations(neighbourhood, player);
                        Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                    });
                    confirmInventory.addItem(13, houseItem, true);
                    for (int i = 0; i < 3 * 9; i++) {
                        if (i < 3 || i >= 9 && i < 12 || i >= 18 && i < 21)
                            confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_CONCRETE, "§a§lCONFIRMER"), true, clickEvent -> {
                                if (house.isMember(player.getUniqueId()) || house.isCoOwner(player.getUniqueId())) {
                                    final Optional<IUser> userOptional = AvatarReturnsAPI.get().getUser(player);
                                    if (userOptional.isPresent()) {
                                        if (userOptional.get().getMoney() >= house.getLeave()) {
                                            house.getCo_owners().remove(player.getUniqueId());
                                            house.getMembers().remove(player.getUniqueId());
                                            house.updateRegions();
                                            userOptional.get().removeMoney(house.getLeave());
                                            Utils.playSound(player, SoundEffects.ITEM_FLINTANDSTEEL_USE);
                                        } else {
                                            Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                            player.sendMessage(Messages.ERROR_MONEY.get());
                                        }
                                    } else
                                        Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                } else
                                    Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                realtorMenu(neighbourhood, player);
                            });
                        else if (i >= 6 && i < 9 || i >= 15 && i < 18 || i >= 24)
                            confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE, "§c§lANNULER"), true, clickEvent -> {
                                Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                listLocations(neighbourhood, player);
                            });
                    }
                    confirmInventory.build(player);
                });
            }
            final Optional<IUser> user = AvatarReturnsAPI.get().getUser(player);
            final Function<Object, ItemStack> function = o -> {
                boolean needToPay = true;
                ItemStack loyer = AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE);
                if (System.currentTimeMillis() - house.getLastRentTime() < house.getRentTime()) {
                    needToPay = false;
                    if (System.currentTimeMillis() - house.getLastRentTime() > house.getRentTime() / 2D)
                        loyer = AvatarReturnsAPI.get().getUtils().getItem(Material.ORANGE_CONCRETE);
                    else
                        loyer = AvatarReturnsAPI.get().getUtils().getItem(Material.GREEN_CONCRETE);
                }
                AvatarReturnsAPI.get().getUtils().modifyItem(loyer, "§cPayer le loyer :");
                final List<String> lore = new ArrayList<>();
                lore.add("");
                final double defaultLoyer = house.getRent();
                final double coOwner = house.getCo_owners().size() * house.getCoOwnerPrice();
                final double member = house.getMembers().size() * house.getMemberPrice();
                final double toPay = Math.ceil((defaultLoyer + coOwner + member) * 100) / 100;
                lore.add("  §7» §9§lTotal : §3" + Utils.change(toPay));
                lore.add("      §7» §9Base : §3" + Utils.change(Math.ceil(defaultLoyer * 100) / 100));
                if (house.getCo_owners().size() > 0)
                    lore.add("      §7» §9Co-proprio : §3" + house.getCo_owners().size() + " * " + Utils.change(Math.ceil(house.getCoOwnerPrice() * 100) / 100));
                if (house.getMembers().size() > 0)
                    lore.add("      §7» §9Membres : §3" + house.getMembers().size() + " * " + Utils.change(Math.ceil(house.getMemberPrice() * 100) / 100));
                lore.add("");
                if (needToPay) {
                    lore.add("§7» §cIl vous reste ");
                    lore.add("§4" + Utils.fromDuration(house.getLastRentTime() + house.getRentTime() + 24 * 3600 * 1000 - System.currentTimeMillis()) + " §cpour payer");
                } else {
                    lore.add("§7» §eVous ne pourrez payer le loyer que dans :");
                    lore.add("§6" + Utils.fromDuration(house.getLastRentTime() + house.getRentTime() - System.currentTimeMillis()));
                }
                AvatarReturnsAPI.get().getUtils().modifyItem(loyer, lore);
                return loyer;
            };
            user.ifPresent(iUser -> inv.addItem(15, function, true, inventoryClickEvent -> {
                if (System.currentTimeMillis() - house.getLastRentTime() > house.getRentTime()) {
                    final double money = house.getRent() + house.getCo_owners().size() * 750D + house.getMembers().size() * 500D;
                    if (iUser.getMoney() >= money) {
                        iUser.removeMoney(money);
                        house.setLastRentTime(System.currentTimeMillis());
                        Utils.playSound(player, SoundEffects.ITEM_FLINTANDSTEEL_USE);
                    } else {
                        Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                        player.sendMessage(Messages.ERROR_MONEY.get());
                    }
                    player.closeInventory();
                } else
                    Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
            }));
            inv.build(player);
        }

        public static void editMember(final Neighbourhood neighbourhood, final Player player, final House house) {
            final InventoryAPI inv = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
            inv.setRefresh(false).setTitle("§3§l" + neighbourhood.getName() + " §8» §7Membres");
            final ItemStack backward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjI1OTliZDk4NjY1OWI4Y2UyYzQ5ODg1MjVjOTRlMTlkZGQzOWZhZDA4YTM4Mjg0YTE5N2YxYjcwNjc1YWNjIn19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(backward, "§6§lRetour");
            final ItemStack add = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2VkZDIwYmU5MzUyMDk0OWU2Y2U3ODlkYzRmNDNlZmFlYjI4YzcxN2VlNmJmY2JiZTAyNzgwMTQyZjcxNiJ9fX0=")
                    .orElse(
                            AvatarReturnsAPI.get().getUtils().getItem(Material.ARMOR_STAND)
                    );
            AvatarReturnsAPI.get().getUtils().modifyItem(add, "§6§lAjouter");
            if (house.getMembers().size() == 0 && house.getCo_owners().size() == 0) {
                inv.addItem(0, backward, true, inventoryClickEvent -> {
                    editHouse(neighbourhood, player, house);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                inv.addItem(4, add, true, inventoryClickEvent -> {
                    addMember(neighbourhood, player, house);
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                });
                inv.build(player);
                return;
            }
            inv.setRefresh(true).setSize(4 * 9);
            final Consumer<InventoryAPI> function = inventoryApi -> {
                if (!house.isOwner(player.getUniqueId()) && !house.isCoOwner(player.getUniqueId())) {
                    inventoryApi.stop();
                    player.closeInventory();
                    return;
                }
                inventoryApi.addItem(27, backward, true, inventoryClickEvent -> {
                    editHouse(neighbourhood, player, house);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                int slot = 0;
                for (final UUID coowner : house.getCo_owners()) {
                    final ItemStack skull = AvatarReturnsAPI.get().getUtils().getSkull(coowner).orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.PLAYER_HEAD));
                    AvatarReturnsAPI.get().getUtils().modifyItem(skull, "§c" + Bukkit.getOfflinePlayer(coowner).getName() + " §4⚝");
                    if (house.isOwner(player.getUniqueId())) {
                        final List<String> lore = new ArrayList<>();
                        lore.add("");
                        lore.add("§7» §6Clic droit pour rétrograder §lCoût : §e" + Utils.change(house.getCoOwnerToMember()));
                        AvatarReturnsAPI.get().getUtils().modifyItem(skull, lore);
                    }
                    inventoryApi.addItem(slot++, skull, true, inventoryClickEvent -> {
                        if (house.isOwner(player.getUniqueId()) && inventoryClickEvent.isRightClick()) {
                            final InventoryAPI confirmInventory = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
                            confirmInventory.setTitle("§3§l" + house.getName() + " §8» §7Confirmation").setSize(3 * 9).setRefresh(false);
                            final List<String> lores = new ArrayList<>();
                            lores.add("");
                            lores.add("  §7» §9§lAction : §3Rétrograder");
                            lores.add("  §7» §9§lPrix : §3" + Utils.change(house.getCoOwnerToMember()));
                            lores.add("");
                            AvatarReturnsAPI.get().getUtils().modifyItem(skull, lores);
                            confirmInventory.addItem(22, backward, true, clickEvent -> {
                                editMember(neighbourhood, player, house);
                                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                            });
                            confirmInventory.addItem(13, skull, true);
                            for (int i = 0; i < 3 * 9; i++) {
                                if (i < 3 || i >= 9 && i < 12 || i >= 18 && i < 21)
                                    confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_CONCRETE, "§a§lCONFIRMER"), true, clickEvent -> {
                                        if (house.isOwner(player.getUniqueId())) {
                                            final Optional<IUser> userOptional = AvatarReturnsAPI.get().getUser(player);
                                            if (userOptional.isPresent()) {
                                                if (userOptional.get().getMoney() >= house.getCoOwnerToMember()) {
                                                    house.getCo_owners().remove(coowner);
                                                    house.getMembers().add(coowner);
                                                    userOptional.get().removeMoney(house.getCoOwnerToMember());
                                                    Utils.playSound(player, SoundEffects.ITEM_FLINTANDSTEEL_USE);
                                                } else {
                                                    Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                                    player.sendMessage(Messages.ERROR_MONEY.get());
                                                }
                                            } else {
                                                Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                                player.sendMessage(Messages.ERROR_OWNER.get());
                                            }
                                        } else {
                                            Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                            player.sendMessage(Messages.ERROR_OWNER.get());
                                        }
                                        editMember(neighbourhood, player, house);
                                    });
                                else if (i >= 6 && i < 9 || i >= 15 && i < 18 || i >= 24)
                                    confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE, "§c§lANNULER"), true, clickEvent -> {
                                        Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                        addMember(neighbourhood, player, house);
                                    });
                            }
                            confirmInventory.build(player);
                            return;
                        }
                        Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                    });
                }
                for (final UUID member : house.getMembers()) {
                    final ItemStack skull = AvatarReturnsAPI.get().getUtils().getSkull(member).orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.PLAYER_HEAD));
                    AvatarReturnsAPI.get().getUtils().modifyItem(skull, "§6" + Bukkit.getOfflinePlayer(member).getName());
                    final List<String> lore = new ArrayList<>();
                    lore.add("");
                    if (house.isOwner(player.getUniqueId()) && house.getCo_owners().size() < Config.getHouse_max_coowners()) {
                        lore.add("§7» §6Clic gauche pour grader §lCoût : §e" + Utils.change(house.getMemberToCoOwner()));
                    }
                    lore.add("§7» §6Clic droit pour supprimer §lCoût : §e" + Utils.change(house.getMemberToStranger()));
                    AvatarReturnsAPI.get().getUtils().modifyItem(skull, lore);
                    inventoryApi.addItem(slot++, skull, true, inventoryClickEvent -> {
                        if ((house.isOwner(player.getUniqueId()) || house.isCoOwner(player.getUniqueId())) && inventoryClickEvent.isRightClick()) {
                            final InventoryAPI confirmInventory = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
                            confirmInventory.setTitle("§3§l" + house.getName() + " §8» §7Confirmation").setSize(3 * 9).setRefresh(false);
                            final List<String> lores = new ArrayList<>();
                            lores.add("");
                            lores.add("  §7» §9§lAction : §3Rétrograder");
                            lores.add("  §7» §9§lPrix : §3" + Utils.change(house.getMemberToStranger()));
                            lores.add("");
                            AvatarReturnsAPI.get().getUtils().modifyItem(skull, lores);
                            confirmInventory.addItem(22, backward, true, clickEvent -> {
                                editMember(neighbourhood, player, house);
                                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                            });
                            confirmInventory.addItem(13, skull, true);
                            for (int i = 0; i < 3 * 9; i++) {
                                if (i < 3 || i >= 9 && i < 12 || i >= 18 && i < 21)
                                    confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_CONCRETE, "§a§lCONFIRMER"), true, clickEvent -> {
                                        if (house.isOwner(player.getUniqueId()) || house.isCoOwner(player.getUniqueId())) {
                                            final Optional<IUser> userOptional = AvatarReturnsAPI.get().getUser(player);
                                            if (userOptional.isPresent()) {
                                                if (userOptional.get().getMoney() >= house.getMemberToStranger()) {
                                                    house.getMembers().remove(member);
                                                    house.updateRegions();
                                                    userOptional.get().removeMoney(house.getMemberToStranger());
                                                    Utils.playSound(player, SoundEffects.ITEM_FLINTANDSTEEL_USE);
                                                } else {
                                                    Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                                    player.sendMessage(Messages.ERROR_MONEY.get());
                                                }
                                            } else
                                                Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                        } else {
                                            Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                            player.sendMessage(Messages.ERROR_OWNER.get());
                                        }
                                        editMember(neighbourhood, player, house);
                                    });
                                else if (i >= 6 && i < 9 || i >= 15 && i < 18 || i >= 24)
                                    confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE, "§c§lANNULER"), true, clickEvent -> {
                                        Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                        addMember(neighbourhood, player, house);
                                    });
                            }
                            confirmInventory.build(player);
                            return;
                        } else if (house.isOwner(player.getUniqueId()) && inventoryClickEvent.isLeftClick() && house.getCo_owners().size() < Config.getHouse_max_coowners()) {
                            final InventoryAPI confirmInventory = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
                            confirmInventory.setTitle("§3§l" + house.getName() + " §8» §7Confirmation").setSize(3 * 9).setRefresh(false);
                            final List<String> lores = new ArrayList<>();
                            lores.add("");
                            lores.add("  §7» §9§lAction : §3Grader");
                            lores.add("  §7» §9§lPrix : §3" + Utils.change(house.getMemberToCoOwner()));
                            lores.add("");
                            AvatarReturnsAPI.get().getUtils().modifyItem(skull, lores);
                            confirmInventory.addItem(22, backward, true, clickEvent -> {
                                editMember(neighbourhood, player, house);
                                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                            });
                            confirmInventory.addItem(13, skull, true);
                            for (int i = 0; i < 3 * 9; i++) {
                                if (i < 3 || i >= 9 && i < 12 || i >= 18 && i < 21)
                                    confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_CONCRETE, "§a§lCONFIRMER"), true, clickEvent -> {
                                        final Optional<IUser> userOptional = AvatarReturnsAPI.get().getUser(player);
                                        if (userOptional.isPresent()) {
                                            if (userOptional.get().getMoney() >= house.getMemberToCoOwner()) {
                                                if (house.isOwner(player.getUniqueId())) {
                                                    house.getMembers().remove(member);
                                                    house.getCo_owners().add(member);
                                                    userOptional.get().removeMoney(house.getMemberToCoOwner());
                                                    Utils.playSound(player, SoundEffects.ITEM_FLINTANDSTEEL_USE);
                                                } else {
                                                    player.sendMessage(Messages.ERROR_OWNER.get());
                                                    Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                                }
                                            } else {
                                                player.sendMessage(Messages.ERROR_MONEY.get());
                                                Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                                            }
                                        } else
                                            Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                        editMember(neighbourhood, player, house);
                                    });
                                else if (i >= 6 && i < 9 || i >= 15 && i < 18 || i >= 24)
                                    confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE, "§c§lANNULER"), true, clickEvent -> {
                                        Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                        addMember(neighbourhood, player, house);
                                    });
                            }
                            confirmInventory.build(player);
                            return;
                        }
                        Utils.playSound(player, SoundEffects.ENTITY_VILLAGER_NO);
                    });
                }
                if (slot < Config.getHouse_max_members())
                    inv.addItem(slot, add, true, inventoryClickEvent -> {
                        addMember(neighbourhood, player, house);
                        Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    });
            };
            inv.setFunction(function);
            inv.build(player);
        }

        public static void addMember(final Neighbourhood neighbourhood, final Player player, final House house) {
            final InventoryAPI inv = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
            inv.setSize(4 * 9).setRefresh(true).setTitle("§3§l" + neighbourhood.getName() + " §8» §7Membres §8» §7Ajout");
            final ItemStack backward = AvatarReturnsAPI.get().getUtils().getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjI1OTliZDk4NjY1OWI4Y2UyYzQ5ODg1MjVjOTRlMTlkZGQzOWZhZDA4YTM4Mjg0YTE5N2YxYjcwNjc1YWNjIn19fQ==")
                    .orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.ARROW));
            AvatarReturnsAPI.get().getUtils().modifyItem(backward, "§6§lRetour");
            final double range = Config.getHouse_range_search_player();
            final Consumer<InventoryAPI> function = inventoryApi -> {
                if (!house.isOwner(player.getUniqueId()) && !house.isCoOwner(player.getUniqueId())) {
                    inventoryApi.stop();
                    player.closeInventory();
                    return;
                }
                final List<Player> players = player.getNearbyEntities(range, range, range).stream()
                        .filter(entity -> entity instanceof Player && !entity.hasMetadata("NPC"))
                        .map(entity -> (Player) entity)
                        .filter(other -> !house.getOwner().get().equals(other.getUniqueId()) && !house.getCo_owners().contains(other.getUniqueId()) && !house.getMembers().contains(other.getUniqueId()))
                        .collect(Collectors.toList());
                inventoryApi.addItem(27, backward, true, inventoryClickEvent -> {
                    editMember(neighbourhood, player, house);
                    Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                });
                for (int slot = 0; slot < 4 * 9; slot++)
                    inventoryApi.clearSlot(slot);
                if (players.size() == 0) {
                    inventoryApi.addItem(13, AvatarReturnsAPI.get().getUtils().getItem(Material.STRUCTURE_VOID, "§cIl n'y a pas de joueur proche"), true);
                    return;
                }
                int slot = 0;
                for (int index = 0; index < players.size(); index++) {
                    if (slot == 27)
                        break;
                    final UUID playerUUID = players.get(index).getUniqueId();
                    final ItemStack skull = AvatarReturnsAPI.get().getUtils().getSkull(playerUUID).orElse(AvatarReturnsAPI.get().getUtils().getItem(Material.PLAYER_HEAD));
                    AvatarReturnsAPI.get().getUtils().modifyItem(skull, "§7§o" + Bukkit.getOfflinePlayer(playerUUID).getName());
                    final List<String> lore = new ArrayList<>();
                    lore.add("");
                    lore.add("§7» §6Clic pour ajouter §lCoût :  §e" + Utils.change(house.getMemberToStranger()));
                    lore.add("");
                    AvatarReturnsAPI.get().getUtils().modifyItem(skull, lore);
                    inventoryApi.addItem(slot, skull, true, inventoryClickEvent -> {
                        Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                        final InventoryAPI confirmInventory = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
                        confirmInventory.setTitle("§3§l" + house.getName() + " §8» §7Confirmation").setSize(3 * 9).setRefresh(false);
                        final List<String> lores = new ArrayList<>();
                        lores.add("");
                        lores.add("  §7» §9§lAction : §3Ajouter");
                        lores.add("  §7» §9§lPrix : §3" + Utils.change(house.getMemberToStranger()));
                        lores.add("");
                        AvatarReturnsAPI.get().getUtils().modifyItem(skull, lores);
                        confirmInventory.addItem(22, backward, true, clickEvent -> {
                            editHouse(neighbourhood, player, house);
                            Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                        });
                        confirmInventory.addItem(13, skull, true);
                        for (int i = 0; i < 3 * 9; i++) {
                            if (i < 3 || i >= 9 && i < 12 || i >= 18 && i < 21)
                                confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_CONCRETE, "§a§lCONFIRMER"), true, clickEvent -> {
                                    final Optional<IUser> userOptional = AvatarReturnsAPI.get().getUser(player);
                                    userOptional.ifPresent(user -> {
                                        if (user.getMoney() >= house.getMemberToStranger() && (house.isOwner(player.getUniqueId()) || house.isCoOwner(player.getUniqueId())) && !house.getMembers().contains(playerUUID)) {
                                            user.removeMoney(house.getMemberToStranger());
                                            house.getMembers().add(playerUUID);
                                        } else {
                                            player.sendMessage(Messages.ERROR_MONEY.get());
                                        }
                                    });
                                    Utils.playSound(player, SoundEffects.ITEM_FLINTANDSTEEL_USE);
                                    editMember(neighbourhood, player, house);
                                });
                            else if (i >= 6 && i < 9 || i >= 15 && i < 18 || i >= 24)
                                confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE, "§c§lANNULER"), true, clickEvent -> {
                                    Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                    addMember(neighbourhood, player, house);
                                });
                        }
                        confirmInventory.build(player);
                        Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                    });
                    slot++;
                }
            };
            inv.setFunction(function);
            inv.build(player);
        }

    }

}
