package fr.avatarreturns.avatarhouse.commands;

import fr.avatarreturns.api.commands.RegisterCommand;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface RegisterSubcommand {

    String[] command();

    int parametersMin() default 0;

    String description() default "";

    String permission() default "";

    String usage() default "";

    RegisterCommand.ExecutorType executorType() default RegisterCommand.ExecutorType.ALL;

}
