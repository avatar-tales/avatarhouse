package fr.avatarreturns.avatarhouse.commands.neighbourhoods;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.inventory.InventoryAPI;
import fr.avatarreturns.avatarhouse.plugin.AvatarHouse;
import fr.avatarreturns.avatarhouse.system.House;
import fr.avatarreturns.avatarhouse.system.Location;
import fr.avatarreturns.avatarhouse.system.Neighbourhood;
import fr.avatarreturns.avatarhouse.system.Parameter;
import fr.avatarreturns.avatarhouse.utils.Utils;
import net.minecraft.server.v1_16_R3.SoundEffects;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.List;

public class Edit {

    public static void handle(final CommandSender sender, final Neighbourhood neighbourhood, final List<String> arguments) {
        if (arguments.size() == 0) {
            sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNotEnoughArguments());
            return;
        }
        switch (arguments.get(0).toLowerCase()) {
            case "location":
                location(sender, neighbourhood);
                return;
            case "delete":
                if (sender instanceof Player) {
                    final Player player = (Player) sender;
                    final InventoryAPI confirmInventory = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
                    confirmInventory.setTitle("§3§l" + neighbourhood.getName() + " §8» §4§lDESTRUCTION").setSize(3 * 9).setRefresh(false);
                    final ItemStack houseItem = AvatarReturnsAPI.get().getUtils().getItem(Material.DIAMOND_BLOCK, "§6§l" + neighbourhood.getName());
                    final List<String> lores = new ArrayList<>();
                    lores.add("");
                    lores.add("  §7» §9§lAction : §4§lDESTRUCTION");
                    lores.add("");
                    AvatarReturnsAPI.get().getUtils().modifyItem(houseItem, lores);
                    confirmInventory.addItem(13, houseItem, true);
                    for (int i = 0; i < 3 * 9; i++) {
                        if (i < 3 || i >= 9 && i < 12 || i >= 18 && i < 21)
                            confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_CONCRETE, "§a§lCONFIRMER"), true, clickEvent -> {
                                neighbourhood.delete();
                                player.closeInventory();
                                Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                            });
                        else if (i >= 6 && i < 9 || i >= 15 && i < 18 || i >= 24)
                            confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE, "§c§lANNULER"), true, clickEvent -> {
                                Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                                player.closeInventory();
                            });
                    }
                    confirmInventory.build(player);
                    Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
                } else {
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNeedPlayer());
                    return;
                }
                return;
            case "houses":
                if (arguments.size() == 1) {
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNotEnoughArguments());
                    return;
                }
                switch (arguments.get(1).toLowerCase()) {
                    case "add":
                        if (arguments.size() == 3) {
                            final Optional<House> houseToAdd = House.getHouse(arguments.get(2));
                            if (houseToAdd.isPresent()) {
                                if (houseToAdd.get().isLink()) {
                                    // TODO: send message is in list
                                    break;
                                }
                                neighbourhood.getHouses().add(houseToAdd.get().getUniqueId());
                                System.err.println("// TODO: send message added");
                                break;
                            }
                        }
                    case "remove":
                        if (arguments.size() == 3) {
                            final Optional<House> houseToRemove = House.getHouse(arguments.get(2));
                            if (houseToRemove.isPresent()) {
                                if (!neighbourhood.getHouses().contains(houseToRemove.get().getUniqueId())) {
                                    // TODO: send message not in list
                                    break;
                                }
                                neighbourhood.getHouses().remove(houseToRemove.get().getUniqueId());
                                System.err.println("// TODO: send message removed");
                                break;
                            }
                        }
                    default:
                        // TODO: send list
                        break;
                }
                return;
            case "set":
                return;
        }
        sender.sendMessage(AvatarReturnsAPI.get().getMessages().getCommandNotFound());
    }

    private static void location(final CommandSender sender, final Neighbourhood neighbourhood) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNeedPlayer());
            return;
        }
        if (neighbourhood.getRealtor().isPresent())
            neighbourhood.removeEntity();
        neighbourhood.setRealtor(Location.convert(((Player) sender).getLocation()));
        neighbourhood.generateEntity();
    }

    public static List<String> completer(final CommandSender sender, final List<String> args) {
        final List<String> completions = new ArrayList<>();
        if (args.size() <= 1)
            return completions;
        final Optional<Neighbourhood> neighbourhood = Neighbourhood.getNeighbourhood(args.get(0));
        if (!neighbourhood.isPresent())
            return completions;
        final List<String> argumentsLvlOne = Arrays.asList("delete", "location", "houses", "set");
        if (args.size() == 2) {
            for(final String string : argumentsLvlOne){
                if(string.toLowerCase().startsWith(args.get(1).toLowerCase()))
                    completions.add(string);
            }
        } else if (args.size() == 3) {
            if (args.get(1).equalsIgnoreCase("houses")) {
                if ("add".startsWith(args.get(2)))
                    completions.add("add");
                if ("remove".startsWith(args.get(2)))
                    completions.add("remove");
            } else if (args.get(1).equalsIgnoreCase("set")) {
                for (final Field field : neighbourhood.get().getClass().getDeclaredFields()) {
                    final Parameter param = field.getAnnotation(Parameter.class);
                    if (param == null)
                        continue;
                    if (Modifier.isFinal(field.getModifiers()) || Modifier.isStatic(field.getModifiers()))
                        continue;
                    if (field.getName().toLowerCase().startsWith(args.get(2).toLowerCase()))
                        completions.add(field.getName());
                }
            }
        } else if (args.size() == 4) {
            if (args.get(1).equalsIgnoreCase("regions")) {
                if (args.get(2).equalsIgnoreCase("remove")) {
                    House.getHouses().stream().filter(house -> !house.isLink())
                            .forEach(house -> {
                                if (house.getName().toLowerCase().startsWith(args.get(3).toLowerCase()))
                                    completions.add(house.getName().contains(" ") ? "\"" + house.getName() + "\"" : house.getName());
                            });
                } else if (args.get(2).equalsIgnoreCase("add")) {
                    for (final UUID uuid : neighbourhood.get().getHouses())
                        House.getHouse(uuid).ifPresent(house -> {
                            if (house.getName().startsWith(args.get(3)))
                                completions.add(house.getName().contains(" ") ? "\"" + house.getName() + "\"" : house.getName());
                        });
                }
            } else if (args.get(1).equalsIgnoreCase("set"))
                completions.add("<value>");
        }
        return completions;
    }

}
