package fr.avatarreturns.avatarhouse.commands;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.RegisterCommand;
import fr.avatarreturns.avatarhouse.permissions.Permissions;
import fr.avatarreturns.avatarhouse.system.House;
import fr.avatarreturns.avatarhouse.system.Neighbourhood;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CommandManager implements ICommandHandler {

    public List<String> convert(final List<String> args, int startIndex) {
        return convert(args.toArray(new String[0]), startIndex);
    }

    public List<String> convert(final String[] args, int startIndex) {
        if (startIndex >= args.length)
            return new ArrayList<>();
        final List<String> result = new ArrayList<>();
        for (int index = startIndex; index < args.length; index++) {
            if (args[index].startsWith("\"")) {
                if (args[index].endsWith("\"")) {
                    result.add(args[index].replace("\"", ""));
                    continue;
                }
                final StringBuilder argument = new StringBuilder();
                do {
                    argument.append(args[index].replace("\"", ""));
                    if (!args[index].endsWith("\"")) {
                        argument.append("_");
                    }
                } while (!args[index++].endsWith("\"") && index < args.length);
                index--;
                result.add(argument.toString().replace("\"", ""));
            } else {
                result.add(args[index]);
            }
        }
        return result;
    }

    @RegisterCommand(command = "/avatarhouse", usage = "/avatarhouse help", parametersMin = 1)
    public void run(final CommandSender sender, final String[] arguments) {
        final List<String> args = convert(arguments, 0);
        switch (args.get(0).toLowerCase()) {
            case "create":
                if (!Permissions.CREATE.hasPermission(sender)) {
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNoPermission());
                    return;
                }
                if (args.get(1).equalsIgnoreCase("house"))
                    fr.avatarreturns.avatarhouse.commands.houses.Create.handle(sender, convert(args, 2));
                else if (args.get(1).equalsIgnoreCase("neighbourhood"))
                    fr.avatarreturns.avatarhouse.commands.neighbourhoods.Create.handle(sender, convert(args, 2));
                return;
            case "kick":
                if (!Permissions.KICK.hasPermission(sender)) {
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNoPermission());
                    return;
                }
                if (args.get(1).equalsIgnoreCase("house")) {
                    final Optional<House> houseOptional = House.getHouse(args.get(2));
                    if (houseOptional.isPresent())
                        fr.avatarreturns.avatarhouse.commands.houses.Kick.handle(sender, houseOptional.get());
                    else return;
                }
                return;
            case "edit":
                if (!Permissions.EDIT.hasPermission(sender)) {
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNoPermission());
                    return;
                }
                if (args.get(1).equalsIgnoreCase("house")) {
                    final Optional<House> houseOptional = House.getHouse(args.get(2));
                    if (houseOptional.isPresent())
                        fr.avatarreturns.avatarhouse.commands.houses.Edit.handle(sender, houseOptional.get(), convert(args, 3));
                    else
                        return;
                } else if (args.get(1).equalsIgnoreCase("neighbourhood")) {
                    final Optional<Neighbourhood> neighbourhoodOptional = Neighbourhood.getNeighbourhood(args.get(2));
                    if (neighbourhoodOptional.isPresent())
                        fr.avatarreturns.avatarhouse.commands.neighbourhoods.Edit.handle(sender, neighbourhoodOptional.get(), convert(args, 3));
                    else
                        return;
                }
                return;
        }
        sender.sendMessage(AvatarReturnsAPI.get().getMessages().getCommandNotFound());
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] arguments) {
        final List<String> args = convert(arguments, 0);
        final List<String> argumentsLvlOne = Arrays.asList("create", "kick", "edit");
        final List<String> completions = new ArrayList<>();
        if (args.size() == 1) {
            for(final String string : argumentsLvlOne){
                if(string.toLowerCase().startsWith(args.get(0).toLowerCase()) && sender.hasPermission("avatar." + string.toLowerCase()))
                    completions.add(string);
            }
        } else if (args.size() == 2) {
            if ("house".startsWith(args.get(1).toLowerCase()))
                completions.add("house");
            if ("neighbourhood".startsWith(args.get(1).toLowerCase())) {
                if (args.get(0).equalsIgnoreCase("create") || args.get(0).equalsIgnoreCase("edit")) {
                    completions.add("neighbourhood");
                }
            }
        } else if (args.size() == 3) {
            if (args.get(0).equalsIgnoreCase("create"))
                completions.add("<name>");
            else {
                if (args.get(1).equalsIgnoreCase("house")) {
                    for (final String name : House.getHouses().stream().map(House::getName).collect(Collectors.toList()))
                        if (name.toLowerCase().startsWith(args.get(2).toLowerCase()))
                            completions.add(name.contains(" ") ? "\"" + name + "\"" : name);
                } else
                    for (final String name : Neighbourhood.getNeighbourhoods().stream().map(Neighbourhood::getName).collect(Collectors.toList()))
                        if (name.toLowerCase().startsWith(args.get(2).toLowerCase()))
                            completions.add(name.contains(" ") ? "\"" + name + "\"" : name);
            }
        } else if (args.size() >= 4) {
            if (args.get(0).equalsIgnoreCase("edit")) {
                if (args.get(1).equalsIgnoreCase("house"))
                    return fr.avatarreturns.avatarhouse.commands.houses.Edit.completer(sender, convert(args, 2));
                else if (args.get(1).equalsIgnoreCase("neighbourhood"))
                    return fr.avatarreturns.avatarhouse.commands.neighbourhoods.Edit.completer(sender, convert(args, 2));
            }
        }
        return completions;
    }
}
