package fr.avatarreturns.avatarhouse.commands.houses;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.api.inventory.InventoryAPI;
import fr.avatarreturns.avatarhouse.messages.Messages;
import fr.avatarreturns.avatarhouse.plugin.AvatarHouse;
import fr.avatarreturns.avatarhouse.system.House;
import fr.avatarreturns.avatarhouse.system.Location;
import fr.avatarreturns.avatarhouse.system.Parameter;
import fr.avatarreturns.avatarhouse.utils.Utils;
import net.minecraft.server.v1_16_R3.SoundEffects;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Edit {

    public static void handle(final CommandSender sender, final House house, final List<String> arguments) {
        if (arguments.size() == 0) {
            sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNotEnoughArguments());
            return;
        }
        switch (arguments.get(0).toLowerCase()) {
            case "delete":
                if (!(sender instanceof Player)) {
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNeedPlayer());
                    return;
                }
                final Player player = (Player) sender;
                final InventoryAPI confirmInventory = AvatarReturnsAPI.get().getInventory(AvatarHouse.get());
                confirmInventory.setTitle("§3§l" + house.getName() + " §8» §4§lDESTRUCTION").setSize(3 * 9).setRefresh(false);
                final ItemStack houseItem = AvatarReturnsAPI.get().getUtils().getItem(Material.DIAMOND_BLOCK, "§6§l" + house.getName());
                final List<String> lores = new ArrayList<>();
                lores.add("");
                lores.add("  §7» §9§lAction : §4§lDESTRUCTION");
                lores.add("");
                AvatarReturnsAPI.get().getUtils().modifyItem(houseItem, lores);
                confirmInventory.addItem(13, houseItem, true);
                for (int i = 0; i < 3 * 9; i++) {
                    if (i < 3 || i >= 9 && i < 12 || i >= 18 && i < 21)
                        confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.LIME_CONCRETE, "§a§lCONFIRMER"), true, clickEvent -> {
                            house.delete();
                            player.closeInventory();
                            Utils.playSound(player, SoundEffects.BLOCK_LEVER_CLICK);
                        });
                    else if (i >= 6 && i < 9 || i >= 15 && i < 18 || i >= 24)
                        confirmInventory.addItem(i, AvatarReturnsAPI.get().getUtils().getItem(Material.RED_CONCRETE, "§c§lANNULER"), true, clickEvent -> {
                            Utils.playSound(player, SoundEffects.BLOCK_GLASS_BREAK);
                            player.closeInventory();
                        });
                }
                confirmInventory.build(player);
                Utils.playSound(player, SoundEffects.UI_BUTTON_CLICK);
            case "location":
                if (!(sender instanceof Player)) {
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNeedPlayer());
                    return;
                }
                final Block targetBlock = ((Player) sender).getTargetBlock(null, 5);
                if (!Utils.isSign(targetBlock.getType())) {
                    sender.sendMessage(Messages.ERROR_SIGN.get());
                    return;
                }
                house.getSign().ifPresent(location -> {
                    if (!targetBlock.getWorld().getUID().equals(location.convert().getWorld().getUID()))
                        house.getRegions().clear();
                });
                house.setSign(Location.convert(targetBlock.getLocation()));
                sender.sendMessage(Messages.INFO_UPDATED.get());
                break;
            case "regions":
                if (!house.getSign().isPresent()) {
                    sender.sendMessage(Messages.ERROR_FIRST_SIGN.get(house.getName()));
                    return;
                }
                if (arguments.size() == 1) {
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNotEnoughArguments());
                    return;
                }
                if (arguments.get(1).equalsIgnoreCase("add")) {
                    if (arguments.size() == 2) {
                        sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNotEnoughArguments());
                        return;
                    }
                    if (!Utils.isRegion(house.getSign().get().convert().getWorld(), arguments.get(2))) {
                        sender.sendMessage(Messages.ERROR_REGION_INVALID.get());
                        return;
                    }
                    if (House.getHouses().stream().anyMatch(houses -> houses.getRegions().contains(arguments.get(2)))) {
                        sender.sendMessage(Messages.ERROR_REGION_EXISTS.get());
                        return;
                    }
                    house.addRegion(arguments.get(2));
                    sender.sendMessage(Messages.INFO_UPDATED.get());
                    return;
                } else if (arguments.get(1).equalsIgnoreCase("remove")) {
                    if (arguments.size() == 2) {
                        sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNotEnoughArguments());
                        return;
                    }
                    if (!house.getRegions().contains(arguments.get(2))) {
                        sender.sendMessage(Messages.ERROR_REGION_FOUND.get());
                        return;
                    }
                    house.removeRegion(arguments.get(2));
                    sender.sendMessage(Messages.INFO_UPDATED.get());
                    return;
                }
            case "set":
                if (arguments.size() <= 2) {
                    sender.sendMessage(AvatarReturnsAPI.get().getMessages().getNotEnoughArguments());
                    return;
                }
                for (final Field field : house.getClass().getDeclaredFields()) {
                    if (!field.getName().equalsIgnoreCase(arguments.get(1)))
                        continue;
                    if (Modifier.isFinal(field.getModifiers()) || Modifier.isStatic(field.getModifiers()))
                        continue;
                    final Parameter param = field.getAnnotation(Parameter.class);
                    if (param == null)
                        continue;
                    Object value;
                    if (field.getType().equals(boolean.class))
                        value = arguments.get(2).equalsIgnoreCase("oui") || arguments.get(2).equalsIgnoreCase("non");
                    else if (field.getType().equals(int.class))
                        value = Integer.parseInt(arguments.get(2));
                    else if (field.getType().equals(double.class))
                        value = Double.parseDouble(arguments.get(2));
                    else if (field.getType().equals(float.class))
                        value = Float.parseFloat(arguments.get(2));
                    else if (field.getType().equals(long.class))
                        value = Long.parseLong(arguments.get(2));
                    else
                        value = arguments.get(2);
                    final Object finalValue = value;
                    Arrays.stream(house.getClass().getMethods())
                            .filter(method -> method.getName().equalsIgnoreCase(param.value()))
                            .forEach(method -> {
                                try {
                                    method.invoke(house, finalValue);
                                    sender.sendMessage(Messages.INFO_UPDATED.get());
                                } catch (Exception ignored) {
                                    sender.sendMessage(Messages.ERROR_INVALID.get());}
                            });

                }
                return;
        }
        sender.sendMessage(AvatarReturnsAPI.get().getMessages().getCommandNotFound());
    }

    public static List<String> completer(final CommandSender sender, final List<String> args) {
        final List<String> completions = new ArrayList<>();
        if (args.size() <= 1)
            return completions;
        final Optional<House> houseOptional = House.getHouse(args.get(0));
        if (!houseOptional.isPresent())
            return completions;
        final List<String> argumentsLvlOne = Arrays.asList("delete", "location", "regions", "set");
        if (args.size() == 2) {
            for(final String string : argumentsLvlOne){
                if(string.toLowerCase().startsWith(args.get(1).toLowerCase()))
                    completions.add(string);
            }
        } else if (args.size() == 3) {
            if (args.get(1).equalsIgnoreCase("regions") && houseOptional.get().getSign().isPresent()) {
                if ("add".startsWith(args.get(2)))
                    completions.add("add");
                if ("remove".startsWith(args.get(2)))
                    completions.add("remove");
            } else if (args.get(1).equalsIgnoreCase("set")) {
                for (final Field field : houseOptional.get().getClass().getDeclaredFields()) {
                    final Parameter param = field.getAnnotation(Parameter.class);
                    if (param == null)
                        continue;
                    if (Modifier.isFinal(field.getModifiers()) || Modifier.isStatic(field.getModifiers()))
                        continue;
                    if (field.getName().toLowerCase().startsWith(args.get(2).toLowerCase()))
                        completions.add(field.getName());
                }
            }
        } else if (args.size() == 4) {
            if (args.get(1).equalsIgnoreCase("regions") && houseOptional.get().getSign().isPresent()) {
                if (args.get(2).equalsIgnoreCase("remove")) {
                    for (final String name : houseOptional.get().getRegions())
                        if (name.startsWith(args.get(3)))
                            completions.add(name);
                } else if (args.get(2).equalsIgnoreCase("add")) {
                    final List<String> regions = Utils.getRegions(houseOptional.get().getSign().get().convert().getWorld());
                    for (final House house : House.getHouses())
                        regions.removeAll(house.getRegions());
                    for (final String name : regions)
                        if (name.startsWith(args.get(3)))
                            completions.add(name);
                }
            } else if (args.get(1).equalsIgnoreCase("set"))
                completions.add("<value>");
        }
        return completions;
    }

}
