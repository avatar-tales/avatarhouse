package fr.avatarreturns.avatarhouse.commands.houses;

import fr.avatarreturns.avatarhouse.system.House;
import org.bukkit.command.CommandSender;

public class Kick {

    public static void handle(final CommandSender sender, final House house) {
        house.setOwner(null);
    }

}
