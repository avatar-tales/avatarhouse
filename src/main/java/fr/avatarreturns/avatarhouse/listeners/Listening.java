package fr.avatarreturns.avatarhouse.listeners;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.avatarhouse.listeners.houses.HouseSign;
import fr.avatarreturns.avatarhouse.listeners.neighbourhoods.Realtor;
import fr.avatarreturns.avatarhouse.plugin.AvatarHouse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.EquipmentSlot;

public class Listening implements Listener {

    @EventHandler
    public void onJoin(final PlayerJoinEvent e) {
    }

    @EventHandler
    public void onBlockBreak(final BlockBreakEvent e) {
        HouseSign.onBlockBreak(e);
    }

    @EventHandler
    public void onInteractAtEntity(final PlayerInteractAtEntityEvent e) {
        if (!e.getHand().equals(EquipmentSlot.HAND))
            return;
        Realtor.interact(e);
    }

    @EventHandler
    public void onEntityDamage(final EntityDamageEvent e) {
        Realtor.damage(e);
    }

    @EventHandler
    public void onEntityDeath(final EntityDeathEvent e) {
        Realtor.kill(e);
    }

    @EventHandler
    public void onEntityDamageEntity(final EntityDamageByEntityEvent e) {
        if (e.getEntity().hasMetadata("NeighbourhoodId")) {
            if (!e.getCause().equals(EntityDamageEvent.DamageCause.ENTITY_ATTACK) || !(e.getDamager() instanceof Player))
                return;
            final PlayerInteractAtEntityEvent event = new PlayerInteractAtEntityEvent((Player) e.getDamager(), e.getEntity(), AvatarReturnsAPI.get().getUtils().from(e.getDamager().getLocation(), e.getEntity().getLocation()), EquipmentSlot.HAND);
            AvatarHouse.get().getServer().getPluginManager().callEvent(event);
        }
    }

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent e) {
        if (!e.getAction().equals(Action.LEFT_CLICK_BLOCK) && !e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getClickedBlock() == null)
            return;
        if (!e.getHand().equals(EquipmentSlot.HAND))
            return;
        HouseSign.interact(e);
    }

}
