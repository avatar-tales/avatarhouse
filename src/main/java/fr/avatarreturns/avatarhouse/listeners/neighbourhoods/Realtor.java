package fr.avatarreturns.avatarhouse.listeners.neighbourhoods;

import fr.avatarreturns.avatarhouse.permissions.Permissions;
import fr.avatarreturns.avatarhouse.plugin.AvatarHouse;
import fr.avatarreturns.avatarhouse.system.Neighbourhood;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

import java.util.Optional;
import java.util.UUID;

public class Realtor {

    public static void interact(final PlayerInteractAtEntityEvent e) {
        if (e.getRightClicked().hasMetadata("NeighbourhoodId")) {
            final Optional<Neighbourhood> optional = Neighbourhood.getNeighbourhood(UUID.fromString(e.getRightClicked().getMetadata("NeighbourhoodId").get(0).asString()));
            if (!optional.isPresent())
                return;
            if (e.getPlayer().isSneaking() && Permissions.EDIT.hasPermission(e.getPlayer())) {
                Neighbourhood.Inventories.editRealtor(optional.get(), e.getPlayer(), 1);
                return;
            }
            Neighbourhood.Inventories.realtorMenu(optional.get(), e.getPlayer());
        }
    }

    public static void damage(final EntityDamageEvent e) {
        if (e.getEntity().hasMetadata("NeighbourhoodId")) {
            e.setCancelled(true);
        }
    }

    public static void kill(final EntityDeathEvent e) {
        if (e.getEntity().hasMetadata("NeighbourhoodId")) {
            Neighbourhood.getNeighbourhood(UUID.fromString(e.getEntity().getMetadata("NeighbourhoodId").get(0).asString()))
                    .ifPresent(neighbourhood -> {
                        neighbourhood.removeEntity();
                        if (AvatarHouse.get().isCanSpawn())
                            neighbourhood.generateEntity();
                    });
        }
    }

}
