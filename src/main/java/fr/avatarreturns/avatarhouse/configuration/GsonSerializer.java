package fr.avatarreturns.avatarhouse.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonSerializer {

    private static GsonSerializer instance;
    private final Gson gson;

    public GsonSerializer() {
        this.gson = new GsonBuilder()
                .disableHtmlEscaping()
                .serializeNulls()
                .setPrettyPrinting()
                .create();
    }

    public static GsonSerializer get() {
        if (instance == null)
            instance = new GsonSerializer();
        return instance;
    }

    public Gson getGson() {
        return gson;
    }
}
